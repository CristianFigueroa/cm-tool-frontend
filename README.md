# CM-TOOL Frontend

Este proyecto es el frontend de CM-TOOL, desarrollado con Angular.

## Requisitos

- Node 14.21.3+

**Nota:** Se recomienda usar [nvm](https://github.com/nvm-sh/nvm) (para sistemas basados en Unix) o [nvm-windows](https://github.com/coreybutler/nvm-windows) (para Windows) para gestionar las versiones de Node.js.

## Configuración y ejecución local

1. Clona el proyecto:

```bash
git clone git@gitlab.com:CristianFigueroa/cm-tool-frontend.git
```

2. -Inicia Git Flow:

```bash
git flow init
```

3. Cambia a la rama `master` con el siguiente comando:

```bash
git switch master
```

4. Instala Yarn globalmente:

```bash
npm install --global yarn
```

5. Instala las dependencias del proyecto:

```bash
yarn install
```

6. Instala la versión específica de npm:

```bash
npm install -g npm@8.19.2
```

7. Ejecuta el proyecto:

```bash
yarn ng serve
```

La aplicación se ejecutará en `localhost:4200`.

## Despliegue en servidor (VPS)

1. **Configura las variables del VPS**: Ve a [Configuración de CI/CD](https://gitlab.com/CristianFigueroa/cm-tool-frontend/-/settings/ci_cd) y configura las variables. Para las variables del servidor, utiliza las credenciales de tu VPS y la clave SSH correspondiente (esto incluye crearla y moverla a autorizadas en al VPS).

2. Instalar unzip:

```bash
sudo apt install unzip
```

3. Actualiza los paquetes de tu sistema e instala Nginx:

```bash
sudo apt-get update
sudo apt-get install nginx
```

4. Crea un nuevo archivo de configuración para tu sitio e inserta el siguiente bloque de configuración en el archivo. Asegúrate de reemplazar con la dirección IP de tu servidor:

```bash
sudo vim  /etc/nginx/sites-available/myapp
```

```nginx
server {
    listen 80;
    server_name IPSERVIDOR;  # Reemplaza IPSERVIDOR con tu dirección IP

    location / {
        root /var/www/IPSERVIDOR/html;  # Reemplaza IPSERVIDOR con tu dirección IP
        try_files $uri $uri/ /index.html;
    }
}
```

5. Crea un enlace simbólico a este archivo

```bash
sudo ln -s /etc/nginx/sites-available/myapp /etc/nginx/sites-enabled/
sudo service nginx restart
```

6. Cambia la propiedad y permisos de la carpeta `/var/www` a tu usuario y grupo:

```bash
sudo chown -R USUARIO:GRUPO /var/www # Reemplaza USUARIO:GRUPO con tus datos
```

7. Inicia el pipeline: Ve a [Pipelines](https://gitlab.com/CristianFigueroa/cm-tool-frontend/-/pipelines) en tu proyecto de GitLab y comienza el pipeline.

8. Accede a la aplicación: Una vez que el pipeline se haya completado con éxito, podrás acceder a la aplicación en `http://ipservidor`. 

## Apartado EMRA

Para que funcione el apartado "EMRA", hay que realizar una petición con un programa de asignatura (cualquiera descargado desde intranet) como un Multipart Form llamando "file" con el archivo.

1. Realiza un POST hacia `http://ipserveremra:8080/uploadFileRA` hasta que responda con "137".
2. Luego, de la misma forma, realiza un POST hacia `http://ipserveremra:8080/uploadFileIndicador`. Esta petición debería devolver "138" con la primera petición.