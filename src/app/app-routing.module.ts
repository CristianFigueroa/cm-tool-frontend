import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginProfesorComponent } from './components/profesor/login/login-profesor.component';
import { IndexComponent } from './components/index/index/index.component';
import { GuardService as guard } from './services/guard.service';
import { ListaAlumnoComponent } from './components/alumno/listar/lista-alumno.component';
import { DetalleAlumnoComponent } from './components/alumno/detalle/detalle-alumno.component';
import { ListaEvaluacionComponent } from './components/evaluacion/lista-evaluacion/lista-evaluacion.component';
import { NuevaEvaluacionComponent } from './components/evaluacion/nueva-evaluacion/nueva-evaluacion.component';
import { EditarEvaluacionComponent } from './components/evaluacion/editar-evaluacion/editar-evaluacion.component';
import { CargarAlumnoComponent } from './components/alumno/cargar/cargar-alumno/cargar-alumno.component';
import { EditarRubricaComponent } from './components/rubrica/editar-rubrica/editar-rubrica.component';
import { DetalleModuloComponent } from './components/modulo/detalle-modulo/detalle-modulo.component';
import { PrincipalAlumnoComponent } from './components/alumno/principal/principal-alumno.component';
import { AdministracionProfesorComponent } from './components/profesor/administracion/administracion-profesor.component';
import { AdministracionSemestreComponent } from './components/semestre/administracion/administracion-semestre.component';
import { VistaAluComponent } from './components/vista-alu/vista-alu.component';
import { LoginAlumnosComponent } from './components/login-alumnos/login-alumnos.component';
import { MostrarRubricaComponent } from './components/rubrica/mostrar-rubrica/mostrar-rubrica.component';

import { ResumenEMRA2Component } from './components/emra2/resumen/resumen-emra2.component';
import { GraficoIndicadorComponent } from './components/emra2/grafico-indicador/grafico-indicador.component';
import { ResumenIndicadoresComponent } from './components/emra2/resumen-indicadores/resumen-indicadores.component';
import { ResumenRasComponent } from './components/emra2/resumen-ras/resumen-ras.component';

const routes: Routes = [
  { path: 'login', component: LoginProfesorComponent },
  { path: 'ingresar', component: LoginAlumnosComponent },
  {
    path: 'emra2',
    component: ResumenEMRA2Component,
    canActivate: [guard],
    data: { expectedRol: ['profesor', 'administrador'] },
  },
  {
    path: 'resumenindicadores',
    component: ResumenIndicadoresComponent,
    canActivate: [guard],
    data: { expectedRol: ['profesor', 'administrador'] },
  },
  {
    path: 'resumenras',
    component: ResumenRasComponent,
    canActivate: [guard],
    data: { expectedRol: ['profesor', 'administrador'] },
  },
  {
    path: 'graficoindicador',
    component: GraficoIndicadorComponent,
    canActivate: [guard],
    data: { expectedRol: ['profesor', 'administrador'] },
  },

  {
    path: 'graficoindicador/:nombreIndicador/:matriculaAlumno',
    component: GraficoIndicadorComponent,
    canActivate: [guard],
    data: { expectedRol: ['profesor', 'administrador'] },
  },

  {
    path: 'alumnos',
    component: PrincipalAlumnoComponent,
    canActivate: [guard],
    data: { expectedRol: ['profesor', 'administrador'] },
  },

  {
    path: 'evaluaciones',
    component: ListaEvaluacionComponent,
    canActivate: [guard],
    data: { expectedRol: ['profesor', 'administrador'] },
  },
  {
    path: 'evaluaciones/nuevo',
    component: NuevaEvaluacionComponent,
    canActivate: [guard],
    data: { expectedRol: ['profesor', 'administrador'] },
  },
  {
    path: 'evaluaciones/editar/:idEvaluacion',
    component: EditarEvaluacionComponent,
    canActivate: [guard],
    data: { expectedRol: ['profesor', 'administrador'] },
  },
  {
    path: 'evaluaciones/:idEvaluacion/alumnos',
    component: ListaAlumnoComponent,
    canActivate: [guard],
    data: { expectedRol: ['profesor', 'administrador'] },
  },

  {
    path: 'rubricas/editar/:matricula/:idEvaluacion',
    component: EditarRubricaComponent,
    canActivate: [guard],
    data: { expectedRol: ['profesor', 'administrador'] },
  },

  {
    path: 'modulo',
    component: DetalleModuloComponent,
    canActivate: [guard],
    data: { expectedRol: ['profesor', 'administrador'] },
  },

  {
    path: 'administracionDocente',
    component: AdministracionProfesorComponent,
    canActivate: [guard],
    data: { expectedRol: ['administrador'] },
  },
  {
    path: 'administracionSemestre',
    component: AdministracionSemestreComponent,
    canActivate: [guard],
    data: { expectedRol: ['administrador'] },
  },

  { path: 'vista/:matricula', component: VistaAluComponent },
  {
    path: 'rubricas/mostrar/:matricula/:idEvaluacion',
    component: MostrarRubricaComponent,
  },

  { path: 'index', component: IndexComponent },
  { path: '**', redirectTo: 'login', pathMatch: 'full' }, // ruta erronea
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
