import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { PlantillaService } from 'src/app/services/plantilla.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

interface BloomStatus {
  text: string;
  value: boolean;
}

interface BloomLevel {
  level: number;
  levelName: string;
  verbs: string[];
}
interface Rubrica {
  ra: string;
  indicadores: string[];
}

@Component({
  selector: 'resumen-indicadores.component',
  templateUrl: './resumen-indicadores.component.html',
  styleUrls: ['./resumen-indicadores.component.scss'],
})
export class ResumenIndicadoresComponent {
  env = environment;

  bloomLevelIndicador: BloomLevel[];
  bloomStatusIndicador: BloomStatus[];
  urlIndicador: string = this.env.urlBackendEMRA + 'asignaturas/138/';
  nombre: string;
  nivel_asignatura: string;
  ciclo: number;
  recomendacionBloomIndicador: string;
  nombresIndicadores: String[];
  indicadorSeleccionado: String[];
  raandIndicadores: Rubrica[];
  indexSelecccionado: number;
  RASeleccionado: String;
  gruposPorRa: any = {};

  constructor(
    private http: HttpClient,
    private toastr: ToastrService,
    private plantillaService: PlantillaService
  ) {}

  ngOnInit() {
    this.cargarNombresIndicadores();

    //datos principales
    this.http.get<any>(this.urlIndicador + 'all').subscribe(
      (response) => {
        if (response) {
          this.nombre = response.asignatura.carrera;
          this.nivel_asignatura = response.asignatura.nivelAsignatura;
          this.ciclo = response.asignatura.ciclo;
        }
      },
      (error) => {
        console.error('Error:', error);
      }
    );
    //datos indicador
    this.http.get<any[]>(this.urlIndicador + 'ras').subscribe(
      (data) => {
        this.processBloomData(data, 'bloomLevelIndicador');
      },
      (error) => {
        console.error('Error:', error);
      }
    );

    //estado indicador
    this.http.get<any[]>(this.urlIndicador + 'ras').subscribe(
      (data) => {
        this.bloomStatusIndicador = data.map((item) => ({
          text: item.text.trim().endsWith('.')
            ? item.text.trim().slice(0, -1).replaceAll('.', '')
            : item.text.trim().replaceAll('.', ''),
          value: item.verb.bloom !== null,
        }));
      },
      (error) => {
        console.error('Error:', error);
      }
    );

    this.http
      .get<Rubrica[]>(this.env.urlBackendCMTOOL + 'rubrica/getraeindicadores')
      .subscribe(
        (data) => {
          this.raandIndicadores = data.map((item) => ({
            ra: this.capitalizeFirstLetter(
              item.ra.toLowerCase().replace('.', '')
            ),
            indicadores: item.indicadores.map((indicador) => {
              indicador = indicador.toLowerCase().replace('.', '');

              // Buscar el indicador en nombresIndicadores
              const nombreCompleto = this.nombresIndicadores.find((nombre) =>
                nombre.toLowerCase().includes(indicador)
              );
              // Si se encuentra una coincidencia, usar el nombre completo. Si no, usar el nombre original.
              return (nombreCompleto ? nombreCompleto : indicador).toString();
            }),
          }));
          // Guardar el primer RA en RASeleccionado
          if (this.raandIndicadores.length > 0) {
            this.indexSelecccionado = 0;

            this.RASeleccionado = this.raandIndicadores[0].ra;
          }
        },
        (error) => {
          console.error('Error:', error);
        }
      );

    //recomendacion indicador
    this.http.get<any>(this.urlIndicador + 'coherencia').subscribe(
      (response) => {
        if (response) {
          this.recomendacionBloomIndicador = response.comentario.replace(
            'RAs',
            'Indicadores'
          );
        } else {
          this.recomendacionBloomIndicador =
            'No se encontró una recomendación válida.';
        }
      },
      (error) => {
        console.error('Error:', error);
      }
    );
    setTimeout(() => {
      this.bloomStatusIndicador.forEach((item) => {
        const ra = this.getRa(this.getNombreCompleto(item.text));
        if (!this.gruposPorRa[ra]) {
          this.gruposPorRa[ra] = [];
        }
        this.gruposPorRa[ra].push(item);
      });
    }, 2000);
  }

  getKeys(obj: object) {
    return Object.keys(obj);
  }
  getRa(indicador: string): string {
    const rubrica = this.raandIndicadores.find((r) =>
      r.indicadores.includes(indicador)
    );
    return rubrica ? rubrica.ra : '';
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  cargarNombresIndicadores(): void {
    this.plantillaService.getNombreIndicadores().subscribe(
      (data) => {
        this.nombresIndicadores = data;
        this.indicadorSeleccionado = this.nombresIndicadores[0].split('-');
      },
      (error) => {
        this.toastr.error(error.error.mensaje, 'Error', {
          timeOut: 3000,
        });
      }
    );
  }
  getNombreCompleto(indicador: string): string {
    indicador = indicador.replace(/\(\s+/g, '('); // Elimina los espacios después de un paréntesis
    const nombreCompleto = this.nombresIndicadores.find((nombre) => {
      nombre = nombre.replace(/^\d+\.\d+\.\s+/, ''); // Elimina el prefijo numérico
      nombre = nombre.replace(/\s+\(/, '('); // Elimina los espacios antes de un paréntesis
      return nombre
        .toLowerCase()
        .trim()
        .includes(indicador.toLowerCase().trim());
    });
    // Si se encuentra una coincidencia, usar el nombre completo. Si no, usar el nombre original.
    return (nombreCompleto ? nombreCompleto : indicador).toString();
  }

  agruparPorRa() {
    const grupos = {};
    this.bloomStatusIndicador.forEach((item) => {
      const ra = this.getRa(this.getNombreCompleto(item.text));
      if (!grupos[ra]) {
        grupos[ra] = [];
      }
      grupos[ra].push(item);
    });
    return grupos;
  }
  processBloomData(data: any[], property: string) {
    const groupedLevels: { [level: number]: BloomLevel } = {};

    data.forEach((obj) => {
      const level = obj.verb.bloom?.id || 0;
      const levelName = obj.verb.bloom?.name || 'No especificado';
      const verbName = obj.verb.name;

      if (!groupedLevels[level]) {
        groupedLevels[level] = {
          level: level,
          levelName: levelName,
          verbs: [],
        };
      }

      groupedLevels[level].verbs.push(verbName);
    });

    this[property] = Object.values(groupedLevels);
  }
  calculateCicloString(): string {
    switch (this.ciclo) {
      case 1:
        return '1 y 2';
      case 2:
        return '3 y 4';
      case 3:
        return '5 y 6';
      default:
        console.log(this.ciclo);
        return 'No especificado';
    }
  }

  getTruePercentage(): number {
    const trueCount = this.bloomStatusIndicador.filter(
      (item) => item.value
    ).length;
    const totalCount = this.bloomStatusIndicador.length;
    return (trueCount / totalCount) * 100;
  }
}
