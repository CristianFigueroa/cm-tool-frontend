import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'navegacion-inferior',
  templateUrl: './barra-navegacion-inferior.component.html',
  styleUrls: ['./barra-navegacion-inferior.component.scss'],
})
export class NavegacionComponent implements OnInit {
  roles: string[];
  isAdmin = false;

  constructor(private tokenService: TokenService) {}

  isLogged = false;

  ngOnInit(): void {
    if (this.tokenService.getToken()) this.isLogged = true;
    else {
      this.isLogged = false;
    }
    this.roles = this.tokenService.getAuthorities();
    this.roles.forEach((rol) => {
      if (rol === 'ROLE_ADMINISTRADOR') {
        this.isAdmin = true;
      }
    });
  }

  onLogOut() {
    this.tokenService.cerrarSesion();
    window.location.reload();
  }
}
