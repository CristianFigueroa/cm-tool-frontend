import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PlantillaService } from 'src/app/services/plantilla.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

interface BloomLevel {
  level: number;
  levelName: string;
  verbs: string[];
}

interface BloomStatus {
  text: string;
  value: boolean;
}

interface Rubrica {
  ra: string;
  indicadores: string[];
}

interface RA {
  name: string;
  level: number;
}

interface Indicador {
  name: string;
  level: number;
}

interface RAeIndicadores {
  ra: RA;
  indicadores: Indicador[];
  maxResta: number;
  minResta: number;
  restaPromedioIndicadores: number;
  cantidadIndicadores: string;
}

@Component({
  selector: 'resumen-emra2.component',
  templateUrl: './resumen-emra2.component.html',
  styleUrls: ['./resumen-emra2.component.scss'],
})
export class ResumenEMRA2Component implements OnInit {
  env = environment;

  bloomLevelsRA: BloomLevel[];
  bloomLevelsIndicadores: BloomLevel[];
  recomendacionBloomRA: string;
  recomendacionBloomIndicadores: string;
  nombre: string;
  nivel_asignatura: string;
  ciclo: number;
  bloomStatusIndicador: BloomStatus[];
  bloomStatusRA: BloomStatus[];
  urlRA: string = this.env.urlBackendEMRA + 'asignaturas/137/';
  urlIndicador: string = this.env.urlBackendEMRA + 'asignaturas/138/';
  raandIndicadores: Rubrica[];
  raandIndicadoresConNivel: RAeIndicadores[];
  RASeleccionado: String;
  nombresIndicadores: String[];

  indicadorSeleccionado: String[];
  indexSelecccionado: number;

  constructor(
    private http: HttpClient,
    private plantillaService: PlantillaService,
    private toastr: ToastrService
  ) {}
  //datos principales, se leen de ras
  ngOnInit() {
    this.cargarNombresIndicadores();

    this.http.get<any>(this.urlRA + 'all').subscribe(
      (response) => {
        if (response) {
          this.nombre = response.asignatura.carrera;
          this.nivel_asignatura = response.asignatura.nivelAsignatura;
          this.ciclo = response.asignatura.ciclo;
        }
      },
      (error) => {
        console.error('Error:', error);
      }
    );

    // ra y sus indicadores
    this.http
      .get<Rubrica[]>(this.env.urlBackendCMTOOL + 'rubrica/getraeindicadores')
      .subscribe(
        (data) => {
          this.raandIndicadores = data.map((item) => ({
            ra: this.capitalizeFirstLetter(
              item.ra.toLowerCase().replace('.', '')
            ),
            indicadores: item.indicadores.map((indicador) => {
              indicador = indicador.toLowerCase().replace('.', '');

              // Buscar el indicador en nombresIndicadores
              const nombreCompleto = this.nombresIndicadores.find((nombre) =>
                nombre.toLowerCase().includes(indicador)
              );
              // Si se encuentra una coincidencia, usar el nombre completo. Si no, usar el nombre original.
              return (nombreCompleto ? nombreCompleto : indicador).toString();
            }),
          }));
          // Guardar el primer RA en RASeleccionado
          if (this.raandIndicadores.length > 0) {
            this.indexSelecccionado = 0;

            this.RASeleccionado = this.raandIndicadores[0].ra;
          }
        },
        (error) => {
          console.error('Error:', error);
        }
      );

    //datos ra
    this.http.get<any[]>(this.urlRA + 'ras').subscribe(
      (data) => {
        this.processBloomData(data, 'bloomLevelsRA');
      },
      (error) => {
        console.error('Error:', error);
      }
    );

    //recomendacion ra
    this.http.get<any>(this.urlRA + 'coherencia').subscribe(
      (response) => {
        if (response) {
          this.recomendacionBloomRA = response.comentario;
        } else {
          this.recomendacionBloomRA =
            'No se encontró una recomendación válida.';
        }
      },
      (error) => {
        console.error('Error:', error);
      }
    );

    //datos indicador
    this.http.get<any[]>(this.urlIndicador + 'ras').subscribe(
      (data) => {
        this.processBloomData(data, 'bloomLevelsIndicadores');
      },
      (error) => {
        console.error('Error:', error);
      }
    );

    //recomendacion indicador
    this.http.get<any>(this.urlIndicador + 'coherencia').subscribe(
      (response) => {
        if (response) {
          this.recomendacionBloomIndicadores = response.comentario;
        } else {
          this.recomendacionBloomIndicadores =
            'No se encontró una recomendación válida.';
        }
      },
      (error) => {
        console.error('Error:', error);
      }
    );

    //estado indicadores
    this.http.get<any[]>(this.urlIndicador + 'ras').subscribe(
      (data) => {
        this.bloomStatusIndicador = data.map((item) => ({
          text: item.text.trim().slice(0, -1).replaceAll('.', ''),
          value: item.verb.bloom !== null,
        }));
      },
      (error) => {
        console.error('Error:', error);
      }
    );

    //estado ras
    this.http.get<any[]>(this.urlRA + 'ras').subscribe(
      (data) => {
        this.bloomStatusRA = data.map((item) => ({
          text: item.text.trim().slice(0, -1).replaceAll('.', ''),
          value: item.verb.bloom !== null,
        }));
      },
      (error) => {
        console.error('Error:', error);
      }
    );

    // Obtener niveles de RA e indicadores juntos
    this.http
      .get<Rubrica[]>(this.env.urlBackendCMTOOL + 'rubrica/getraeindicadores')
      .subscribe(
        (data) => {
          this.raandIndicadoresConNivel = data.map((item) => {
            const ra: RA = {
              name: item.ra.toLowerCase().replace('.', ''),
              level: 0,
            };

            const indicadores: Indicador[] = item.indicadores.map(
              (indicador) => ({
                name: indicador.toLowerCase().replace(/\./g, ''),
                level: 0,
              })
            );

            return {
              ra: ra,
              indicadores: indicadores,
            } as RAeIndicadores;
          });

          this.fetchNivelesRAeIndicadores();
        },
        (error) => {
          console.error('Error:', error);
        }
      );
  }
  getNombreCompleto(indicador: string): string {
    const nombreCompleto = this.nombresIndicadores.find((nombre) =>
      nombre.toLowerCase().includes(indicador.toLowerCase())
    );
    // Si se encuentra una coincidencia, usar el nombre completo. Si no, usar el nombre original.
    return (nombreCompleto ? nombreCompleto : indicador).toString();
  }
  cargarNombresIndicadores(): void {
    this.plantillaService.getNombreIndicadores().subscribe(
      (data) => {
        this.nombresIndicadores = data;
        this.indicadorSeleccionado = this.nombresIndicadores[0].split('-');
      },
      (error) => {
        this.toastr.error(error.error.mensaje, 'Error', {
          timeOut: 3000,
        });
      }
    );
  }

  cambiarIndicador(nombreRA: String, i: number): void {
    //cambiar indicador
    this.RASeleccionado = nombreRA; // Guardar el nombre del RA
    this.indexSelecccionado = i;
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  fetchNivelesRAeIndicadores() {
    // Obtener niveles de RA
    this.http.get<any[]>(this.urlRA + 'ras').subscribe(
      (dataRA) => {
        this.raandIndicadoresConNivel.forEach((raeIndicador) => {
          const raText = raeIndicador.ra.name.toLowerCase().replace(/\./g, '');
          const raLevel = dataRA.find((item) =>
            item.text.toLowerCase().includes(raText)
          );
          if (raLevel) {
            raeIndicador.ra.level = raLevel.verb.bloom.id;
          }
        });

        // Asegurarse de que calcularMaxMinResta y calcularProemdioResta se ejecutan después de que los datos se han cargado completamente
        setTimeout(() => {
          this.calcularMaxMinResta();
          this.calcularProemdioResta();
        }, 1000); // Retrasar la ejecución en 1 segundo
      },
      (error) => {
        console.error('Error:', error);
      }
    );

    // Obtener niveles de indicadores
    this.http.get<any[]>(this.urlIndicador + 'ras').subscribe(
      (dataIndicador) => {
        this.raandIndicadoresConNivel.forEach((raeIndicador) => {
          raeIndicador.indicadores.forEach((indicador) => {
            const indicadorText = indicador.name
              .toLowerCase()
              .replace(/[.\(\)]/g, '');

            const indicadorLevel = dataIndicador.find((item) =>
              item.text
                .toLowerCase()
                .replace(/[.\(\)]/g, '')
                .includes(indicadorText)
            );
            if (indicadorLevel) {
              indicador.level = indicadorLevel.verb.bloom.id;
            }
          });
        });

        this.calcularMaxMinResta();
        this.calcularProemdioResta();
      },
      (error) => {
        console.error('Error:', error);
      }
    );
  }

  calcularMaxMinResta() {
    this.raandIndicadoresConNivel.forEach((raeIndicador) => {
      let raLevel = raeIndicador.ra.level;

      let maxIndicadorLevel = 0;
      let minIndicadorLevel = 6; // Inicializar con el nivel máximo posible

      raeIndicador.indicadores.forEach((indicador) => {
        let indicadorLevel = indicador.level;

        if (indicadorLevel > maxIndicadorLevel) {
          maxIndicadorLevel = indicadorLevel;
        }
        if (indicadorLevel < minIndicadorLevel) {
          minIndicadorLevel = indicadorLevel;
        }
      });

      raeIndicador.maxResta = raLevel - maxIndicadorLevel;
      raeIndicador.minResta = raLevel - minIndicadorLevel;
    });
  }

  getTruePercentage(read: string): number {
    if (read == 'bloomStatusIndicador') {
      const trueCount = this.bloomStatusIndicador.filter(
        (item) => item.value
      ).length;
      const totalCount = this.bloomStatusIndicador.length;
      return (trueCount / totalCount) * 100;
    } else {
      const trueCount = this.bloomStatusRA.filter((item) => item.value).length;
      const totalCount = this.bloomStatusRA.length;
      return (trueCount / totalCount) * 100;
    }
  }

  processBloomData(data: any[], property: string) {
    const groupedLevels: { [level: number]: BloomLevel } = {};

    data.forEach((obj) => {
      const level = obj.verb.bloom?.id || 0;
      const levelName = obj.verb.bloom?.name || 'No especificado';
      const verbName = obj.verb.name;

      if (!groupedLevels[level]) {
        groupedLevels[level] = {
          level: level,
          levelName: levelName,
          verbs: [],
        };
      }

      groupedLevels[level].verbs.push(verbName);
    });

    this[property] = Object.values(groupedLevels);
  }

  calculateCicloString(): string {
    switch (this.ciclo) {
      case 1:
        return '1 y 2';
      case 2:
        return '3 y 4';
      case 3:
        return '5 y 6';
      default:
        console.log(this.ciclo);
        return 'No especificado';
    }
  }

  calcularProemdioResta(): void {
    this.raandIndicadoresConNivel.forEach((raeIndicador) => {
      let sumIndicadorLevels = 0;

      raeIndicador.indicadores.forEach((indicador) => {
        sumIndicadorLevels += indicador.level;
      });

      let promedioIndicadores =
        sumIndicadorLevels / raeIndicador.indicadores.length;
      raeIndicador.restaPromedioIndicadores =
        raeIndicador.ra.level - promedioIndicadores;
      if (raeIndicador.indicadores.length > 7) {
        raeIndicador.cantidadIndicadores =
          'Demasiados indicadores, se recomienda entre 4-7 indicadores por RA';
      } else if (raeIndicador.indicadores.length < 4) {
        raeIndicador.cantidadIndicadores =
          'Pocos indicadores, se recomienda entre 4-7 indicadores por RA';
      } else if (
        raeIndicador.indicadores.length >= 4 &&
        raeIndicador.indicadores.length <= 7
      ) {
        raeIndicador.cantidadIndicadores = 'Cantidad de indicadores correcta';
      }
    });
  }

  calculateAverageNivelIndicadores(): number {
    let totalLevels = 0;
    let sumLevels = 0;

    for (const bloomLevel of this.bloomLevelsIndicadores) {
      let actualLevel = bloomLevel.level;
      let countLevel = bloomLevel.verbs.length;
      sumLevels += actualLevel * countLevel;
      totalLevels += countLevel;
    }

    if (totalLevels === 0) {
      return 0;
    }

    return sumLevels / totalLevels;
  }

  calculateAverageNivelRAs(): number {
    let totalLevels = 0;
    let sumLevels = 0;
    for (const bloomLevel of this.bloomLevelsRA) {
      let actualLevel = bloomLevel.level;
      let countLevel = bloomLevel.verbs.length;
      sumLevels += actualLevel * countLevel;
      totalLevels += countLevel;
    }

    if (totalLevels === 0) {
      return 0;
    }
    return sumLevels / totalLevels;
  }
}
