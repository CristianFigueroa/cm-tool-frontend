import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { ToastrService } from 'ngx-toastr';
import { forkJoin, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Alumno } from 'src/app/model/alumno';
import { Categoria } from 'src/app/model/categoria';
import { Evaluacion } from 'src/app/model/evaluacion';
import { Indicador } from 'src/app/model/indicador';
import { Rubrica } from 'src/app/model/rubrica';
import { AlumnoService } from 'src/app/services/alumno.service';
import { EvaluacionService } from 'src/app/services/evaluacion.service';
import { PlantillaService } from 'src/app/services/plantilla.service';
import { RubricaService } from 'src/app/services/rubrica.service';
import { environment } from 'src/environments/environment';

interface RaIndicador {
  ra: string;
  indicadores: string[];
}
@Component({
  selector: 'grafico-indicador.component',
  templateUrl: './grafico-indicador.component.html',
  styleUrls: ['./grafico-indicador.component.scss'],
})
export class GraficoIndicadorComponent implements OnInit {
  env = environment;
  //valores generales
  modulo: Alumno[];
  evaluaciones: Evaluacion[];
  rubricas: Rubrica[];
  indicadores: Indicador[];
  plantillaIndicadores = {};
  plantillaCategorias = {};
  categorias: Categoria[] = [];
  evaluacion: Evaluacion;

  //valores alumno
  nombresIndicadores: String[];
  indicadorSeleccionado: String[];
  RASeleccionado: String;
  alumnoSeleccionado: Alumno;
  indexSelecccionado: number;
  descripcionesEvaluaciones: String[] = []; // Variable para almacenar las descripciones
  puntajesActuales: [number, String][] = []; //Almacenar puntaje para alumno y indicador actual y nombre evaluacion
  informacionActual: [number, String, number, number][] = []; //Almacenar puntaje para alumno y indicador actual, nombre evaluacion, numero de plantilla y puntaje maximo obtenible

  //valores grafico
  lineChartLabels: Label[] = [];
  data = [];
  lineChartData: ChartDataSets[] = [];
  lineChartOptions: ChartOptions = {};
  lineChartColors: Color[] = [];
  lineChartLegend: boolean;
  lineChartType: String;
  mostrarGrafico: boolean = false;
  logradoData: number[] = [];
  maximoData: number[] = [];
  esperadoData: number[] = [];
  newData: any;
  //mensaje de indicador no evaluado
  mensaje: string;
  //estado de carga del grafico
  cargando: boolean = false;

  raandIndicadores: RaIndicador[];

  constructor(
    private toastr: ToastrService,
    private alumnoService: AlumnoService,
    private evaluacionService: EvaluacionService,
    private rubricaService: RubricaService,
    private plantillaService: PlantillaService,
    private route: ActivatedRoute,
    private http: HttpClient
  ) {
    // Accede a los parámetros de la ruta
    this.route.params.subscribe((params) => {
      if (params['nombreIndicador']) {
        this.indicadorSeleccionado = params['nombreIndicador'];
        this.alumnoSeleccionado = new Alumno(params['matriculaAlumno'], '', 1);
      }
    });
  }

  ngOnInit(): void {
    this.cargarAlumnos();
    this.cargarEvaluaciones();
    this.cargarNombresIndicadores();
    this.leerDescripcionesEvaluacionesOrdenadas();

    if (this.alumnoSeleccionado) {
      this.cargarDatosGrafico();
    } else {
      // No hay alumno seleccionado, no es necesario borrar los datos
      this.mostrarGraficoFunc(false);
    }
    this.getRAeIndicadores();
  }

  mostrarGraficoFunc(mostrar: boolean) {
    setTimeout(() => {
      this.cargando = false;
    }, 300);
    if (mostrar) {
      this.mostrarGrafico = true;
    } else {
      this.mostrarGrafico = false;
    }
  }

  generarCanvas(): void {
    // Inicializa arrays vacíos para los datos de Logrado y maximo
    this.logradoData = [];
    this.maximoData = [];
    this.esperadoData = [];
    this.data = [];
    this.newData = [];
    // Recorre this.informacionActual utilizando forEach

    this.informacionActual.forEach((item) => {
      setTimeout(() => {
        this.logradoData.push(item[0]);
        this.maximoData.push(item[3]);
        this.esperadoData.push(item[3] * 0.5);
      }, 400);
    });

    this.data = [
      { data: this.logradoData, label: 'Puntaje Logrado' },
      { data: this.maximoData, label: 'Puntaje Máximo Obtenible' },
      { data: this.esperadoData, label: 'Puntaje Esperado' },
    ];
    setTimeout(() => {
      let maximoEsperadoData = this.maximoData.map(
        (value, index) => value - this.esperadoData[index]
      );
      this.newData = [
        { data: this.logradoData, label: 'Puntaje Logrado', fill: false },
        {
          data: maximoEsperadoData,
          label: 'Puntaje Esperado',
          fill: '+1',
        },
        {
          data: this.maximoData,
          label: 'Puntaje Máximo Obtenible',
          fill: false,
        }, // Esta línea ha sido eliminada
      ].map((item) => {
        let cumulativeSum = 0;
        let newDataArray = item.data.map((value: number) => {
          cumulativeSum += value;
          return cumulativeSum;
        });
        return {
          data: newDataArray,
          label: item.label,
          fill: item.fill,
        };
      });
      this.lineChartData = this.newData;
    }, 400);

    // Labels shown on the x-axis
    const labels = this.informacionActual.map((item) => item[1]);

    // Define chart labels
    this.lineChartLabels = labels as Label[];

    // Define chart options
    this.lineChartOptions = {
      responsive: true,
      elements: {
        line: {
          tension: 0, // Hace que las líneas sean rectas
        },
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true, // Esto hará que el eje Y comience en 0
            },
          },
        ],
      },
    };

    // Define colors of chart segments
    this.lineChartColors = [
      {
        // Logrado
        backgroundColor: 'rgba(0, 10, 255, 0.65)', // azul aún más oscuro
        borderColor: 'rgba(0, 10, 255, 0.65)', // borde azul aún más oscuro
      },
      {
        // Esperado
        backgroundColor: 'rgba(160, 255, 152, 0.65)', // verde más claro
        borderColor: 'rgba(160, 255, 152, 0.65)', // borde verde más claro
      },
      {
        // Maximo
        backgroundColor: 'rgba(100, 200, 100, 0.55)', // verde aún más claro
        borderColor: 'rgba(100, 200, 100, 0.55)', // borde verde aún más claro
      },
    ];

    // Set true to show legends
    this.lineChartLegend = true;

    // Define the type of chart
    this.lineChartType = 'line';
  }

  getRAeIndicadores() {
    this.http
      .get<RaIndicador[]>(
        this.env.urlBackendCMTOOL + 'rubrica/getraeindicadores'
      )
      .subscribe(
        (data) => {
          let indicadoresUsados = [];
          this.raandIndicadores = data.map((item) => ({
            ra: this.capitalizeFirstLetter(
              item.ra.toLowerCase().replace('.', '')
            ),
            indicadores: item.indicadores.map((indicador) => {
              indicador = indicador.toLowerCase().replace('.', '');
              // Buscar el indicador en nombresIndicadores
              let nombreCompleto;
              for (let nombre of this.nombresIndicadores) {
                if (
                  nombre.toLowerCase().includes(indicador) &&
                  !indicadoresUsados.includes(nombre)
                ) {
                  nombreCompleto = nombre;
                  indicadoresUsados.push(nombre);
                  break;
                }
              }
              // Si se encuentra una coincidencia, usar el nombre completo. Si no, usar el nombre original.
              return (nombreCompleto ? nombreCompleto : indicador).toString();
            }),
          }));
          // Guardar el primer RA en RASeleccionado
          if (this.raandIndicadores.length > 0) {
            this.indexSelecccionado = 0;
            this.RASeleccionado = this.raandIndicadores[0].ra;
          }
        },
        (error) => {
          console.error('Error:', error);
        }
      );
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  getCategorias() {
    this.categorias = [];
    if (this.indicadores)
      this.indicadores.forEach((x) => {
        if (
          !this.categorias.find((y) => y.idCategoria == x.categoria.idCategoria)
        ) {
          this.categorias.push(x.categoria);
        }
      });
    this.plantillaCategorias[this.evaluacion.idEvaluacion] = this.categorias;
  }

  cargarEvaluaciones(): void {
    this.evaluacionService.list().subscribe(
      (data) => {
        this.evaluaciones = data;
      },
      (error) => {
        this.toastr.error(error.error.mensaje, 'Error', {
          timeOut: 3000,
        });
      }
    );
  }

  cargarRubricas(): void {
    this.rubricaService.list().subscribe(
      (data) => {
        this.rubricas = data;
      },
      (error) => {
        this.toastr.error(error.error.mensaje, 'Error', {
          timeOut: 3000,
        });
      }
    );
  }

  cargarAlumnos(): void {
    this.alumnoService.list().subscribe(
      (data) => {
        this.modulo = data;
      },
      (error) => {
        this.toastr.error(error.error.mensaje, 'Error', {
          timeOut: 3000,
        });
      }
    );
  }

  cargarNombresIndicadores(): void {
    this.plantillaService.getNombreIndicadores().subscribe(
      (data) => {
        this.nombresIndicadores = data;
        this.indicadorSeleccionado = this.nombresIndicadores[0].split('-');
      },
      (error) => {
        this.toastr.error(error.error.mensaje, 'Error', {
          timeOut: 3000,
        });
      }
    );
  }

  cargarDatosGrafico() {
    setTimeout(() => {
      this.leerPuntaje(
        this.indicadorSeleccionado.toString(),
        this.alumnoSeleccionado.matriculaAlumno
      );
    }, 400);
    setTimeout(() => {
      this.leerPlantilla();
    }, 800);
    setTimeout(() => {
      this.leerPuntajeMaximoObtenible();
    }, 1200);
    setTimeout(() => {
      this.generarCanvas();
    }, 1600);
    setTimeout(() => {
      this.mostrarGraficoFunc(true);
    }, 2000);
  }

  cambiarIndicador(nombreIndicador: String, nombreRA: String, i: number): void {
    //cambiar indicador
    this.cargando = true;
    this.limpiarValores();
    this.indicadorSeleccionado = nombreIndicador.split('-');
    this.RASeleccionado = nombreRA; // Guardar el nombre del RA
    this.indexSelecccionado = i;
    if (this.alumnoSeleccionado) {
      this.cargarDatosGrafico();
    } else {
      // No hay alumno seleccionado, no es necesario borrar los datos
      this.mostrarGraficoFunc(false);
    }
  }

  seleccionarAlumno(alumno: Alumno): void {
    //cambiar alumno
    this.cargando = true;

    this.limpiarValores();
    this.alumnoSeleccionado = alumno;
    if (this.alumnoSeleccionado) {
      this.cargarDatosGrafico();
    } else {
      // No hay alumno seleccionado, no es necesario borrar los datos
      this.mostrarGraficoFunc(false);
    }
  }

  limpiarValores(): void {
    this.puntajesActuales.forEach((element, index) => {
      if (typeof element[0] === 'number') {
        // Si el primer elemento (número) es un número, reemplázalo por 0.
        this.puntajesActuales[index][0] = 0;
      }
    });
    this.informacionActual.forEach((element, index) => {
      if (typeof element[0] === 'number') {
        // Si el primer elemento (número) es un número, reemplázalo por 0.
        this.informacionActual[index][0] = 0;
      }
      if (typeof element[3] === 'number') {
        // Si el tercer elemento (número) es un número, reemplázalo por 0.
        this.informacionActual[index][3] = 0;
      }
    });
  }

  leerPuntaje(nombreIndicador: string, matricula: string): void {
    this.evaluacionService
      .findEvaluacionPuntajePorIndicadorMatricula(nombreIndicador, matricula)
      .subscribe(
        (data) => {
          // Verificar si data es null
          if (data === null) {
            // En este caso, no hagas nada y mantén los puntajes en 0
            return;
          } else {
            // Recorre las evaluaciones existentes en this.puntajesActuales y actualiza los puntajes
            this.puntajesActuales.forEach((puntaje, index) => {
              const evaluacionEncontrada = data.find(
                (evaluacion) => evaluacion[1] === puntaje[1]
              );
              if (evaluacionEncontrada) {
                this.puntajesActuales[index] = evaluacionEncontrada;
              }
            });
          }
        },
        (error) => {
          this.toastr.error(error.error.mensaje, 'Error', {
            timeOut: 3000,
          });
        }
      );
  }

  leerDescripcionesEvaluacionesOrdenadas() {
    this.evaluacionService.list().subscribe(
      (data) => {
        // Copiar el arreglo de evaluaciones para no modificar el original
        const evaluacionesOrdenadas = [...data];
        // Crear una función de comparación personalizada para ordenar las fechas
        function compararFechas(a: any, b: any): number {
          // Reformatea las fechas al formato "YYYY/MM/DD" para que sean comparables
          const fechaA: Date = new Date(
            a.fechaEvaluacion.split('-').reverse().join('/')
          );
          const fechaB: Date = new Date(
            b.fechaEvaluacion.split('-').reverse().join('/')
          );
          return fechaA.getTime() - fechaB.getTime();
        }

        // Ordenar el arreglo de evaluaciones utilizando la función de comparación personalizada
        evaluacionesOrdenadas.sort(compararFechas);
        // Crear el arreglo puntajesActuales con puntajes iniciales de 0 y descripciones
        this.puntajesActuales = evaluacionesOrdenadas.map((evaluacion) => [
          0,
          evaluacion.descripcion,
        ]);
      },
      (error) => {
        this.toastr.error(error.error.mensaje, 'Error', {
          timeOut: 3000,
        });
      }
    );
  }

  leerPlantilla() {
    this.puntajesActuales.forEach((evaluacion, index) => {
      const evaluacionNombre = evaluacion[1]; // Nombre de la evaluación
      this.evaluacionService.findEvaluacionPorDesc(evaluacionNombre).subscribe(
        (data) => {
          // Verificar si data es null
          if (data === null) {
            // En este caso, no hagas nada y mantén los puntajes en 0
            //no hay plantilla
          } else {
            // Encuentra la evaluación correspondiente en this.puntajesActuales
            const indicePuntaje = this.puntajesActuales.findIndex(
              (puntaje) => puntaje[1] === evaluacionNombre
            );
            if (indicePuntaje !== -1) {
              // Actualiza los datos en informacionActual
              this.informacionActual[indicePuntaje] = [
                this.puntajesActuales[indicePuntaje][0], // puntaje actual
                evaluacionNombre, // nombre de evaluación
                data[0].numeroPlantilla, // número de plantilla
                0, // o cualquier otro valor numérico
              ];
            }
          }
        },
        (error) => {
          this.toastr.error(error.error.mensaje, 'Error', {
            timeOut: 3000,
          });
        }
      );
    });
  }

  leerPuntajeMaximoObtenible() {
    let todosMaximosCero = true;
    const observables = this.informacionActual.map((informacion, index) => {
      const numeroPlantilla = informacion[2];
      return this.plantillaService.getByNivel(numeroPlantilla).pipe(
        tap((data) => {
          if (data === null) {
            //no se encontro
          } else {
            const indicadorEncontrado = data.find(
              (indicador) =>
                indicador.nombreIndicador === this.indicadorSeleccionado[0]
            );
            if (indicadorEncontrado) {
              // Modifica el último valor de informacionActual (this.informacionActual[index][3]) a 4
              this.informacionActual[index][3] = 4;
              todosMaximosCero = false;
            } else {
              // Establece un valor predeterminado de 0 si no se encuentra el indicador
              this.informacionActual[index][3] = 0;
            }
          }
        }),
        catchError((error) => {
          this.toastr.error(error.error.mensaje, 'Error', {
            timeOut: 3000,
          });
          return of(null);
        })
      );
    });

    forkJoin(observables).subscribe(() => {
      if (todosMaximosCero) {
        this.mensaje = 'El indicador seleccionado aún no ha sido evaluado.';
      } else {
        this.mensaje = null;
      }
    });
  }
}
