import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Location } from '@angular/common'; // Importa el servicio Location
import { environment } from 'src/environments/environment';

interface BloomStatus {
  text: string;
  value: boolean;
}

interface BloomLevel {
  level: number;
  levelName: string;
  verbs: string[];
}

@Component({
  selector: 'resumen-ras.component',
  templateUrl: './resumen-ras.component.html',
  styleUrls: ['./resumen-ras.component.scss'],
})
export class ResumenRasComponent {
  env = environment;

  bloomLevelsRA: BloomLevel[];
  bloomStatusRA: BloomStatus[];
  urlRA: string = this.env.urlBackendEMRA + 'asignaturas/137/';
  nombre: string;
  nivel_asignatura: string;
  ciclo: number;
  recomendacionBloomRA: string;
  constructor(private http: HttpClient, private location: Location) {} // Inyecta el servicio Location

  ngOnInit() {
    //datos principales
    this.http.get<any>(this.urlRA + 'all').subscribe(
      (response) => {
        if (response) {
          this.nombre = response.asignatura.carrera;
          this.nivel_asignatura = response.asignatura.nivelAsignatura;
          this.ciclo = response.asignatura.ciclo;
        }
      },
      (error) => {
        console.error('Error:', error);
      }
    );
    //datos ra
    this.http.get<any[]>(this.urlRA + 'ras').subscribe(
      (data) => {
        this.processBloomData(data, 'bloomLevelsRA');
      },
      (error) => {
        console.error('Error:', error);
      }
    );

    //estado ra
    this.http.get<any[]>(this.urlRA + 'ras').subscribe(
      (data) => {
        this.bloomStatusRA = data.map((item) => ({
          text: item.text.trim().slice(0, -1).replaceAll('.', ''),
          value: item.verb.bloom !== null,
        }));
      },
      (error) => {
        console.error('Error:', error);
      }
    );

    //recomendacion ra
    this.http.get<any>(this.urlRA + 'coherencia').subscribe(
      (response) => {
        if (response) {
          this.recomendacionBloomRA = response.comentario;
        } else {
          this.recomendacionBloomRA =
            'No se encontró una recomendación válida.';
        }
      },
      (error) => {
        console.error('Error:', error);
      }
    );
  }

  // Agrega el método volverAtras
  volverAtras() {
    this.location.back();
  }

  capitalizeFirstLetter(text: string) {
    return text.charAt(0).toUpperCase() + text.slice(1);
  }

  processBloomData(data: any[], property: string) {
    const groupedLevels: { [level: number]: BloomLevel } = {};

    data.forEach((obj) => {
      const level = obj.verb.bloom?.id || 0;
      const levelName = obj.verb.bloom?.name || 'No especificado';
      const verbName = obj.verb.name;

      if (!groupedLevels[level]) {
        groupedLevels[level] = {
          level: level,
          levelName: levelName,
          verbs: [],
        };
      }

      groupedLevels[level].verbs.push(verbName);
    });

    this[property] = Object.values(groupedLevels);
  }
  calculateCicloString(): string {
    switch (this.ciclo) {
      case 1:
        return '1 y 2';
      case 2:
        return '3 y 4';
      case 3:
        return '5 y 6';
      default:
        console.log(this.ciclo);
        return 'No especificado';
    }
  }

  getTruePercentage(): number {
    const trueCount = this.bloomStatusRA.filter((item) => item.value).length;
    const totalCount = this.bloomStatusRA.length;
    return (trueCount / totalCount) * 100;
  }
}
