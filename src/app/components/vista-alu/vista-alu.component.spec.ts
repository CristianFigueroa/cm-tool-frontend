import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaAluComponent } from './vista-alu.component';

describe('VistaAluComponent', () => {
  let component: VistaAluComponent;
  let fixture: ComponentFixture<VistaAluComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VistaAluComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaAluComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
