import { Component, OnInit , Output, ViewChild} from '@angular/core';
import { Alumno } from 'src/app/model/alumno';
import { ToastrService } from 'ngx-toastr';
import { Evaluacion } from 'src/app/model/evaluacion';
import { EvaluacionService } from 'src/app/services/evaluacion.service';
import { AlumnoService } from 'src/app/services/alumno.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-vista-alu',
  templateUrl: './vista-alu.component.html',
  styleUrls: ['./vista-alu.component.scss']
})
export class VistaAluComponent implements OnInit {
  tipoEvaluacion: number = 0;

  @Output() alumnoSeleccionado: Alumno;
  
  evaluaciones: Evaluacion[];
  evaluacionSeleccionada: Evaluacion;
  modulo: number;
  
  matricula: string;

  constructor(
    private toastr: ToastrService,
    private evaluacionService: EvaluacionService,
    private alumnoService: AlumnoService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => this.matricula = params['matricula']);
    this.cargarEvaluaciones();
    this.cargarAlumnos();
    
    
  }


  cambiarEvaluacion(tipoEvaluacion: number, evaluacion: any) {
    this.tipoEvaluacion = tipoEvaluacion;
    this.evaluacionSeleccionada = evaluacion;
    if (this.tipoEvaluacion == 0) {
      if (this.alumnoSeleccionado) {    // no eval, si alum
        this.evaluacionSeleccionada = null;
      } else {                          // no eval, no alum
        this.alumnoSeleccionado = null;
      }
    }
    if (this.tipoEvaluacion != 0) {
      if (this.alumnoSeleccionado) {    // si eval, si alum 

      } else {                          // si eval, no alum
        this.alumnoSeleccionado = null;
      }
    }
  }

  cargarAlumnos(): void {
    this.alumnoService.getById(this.matricula).subscribe(
      data => {
        this.alumnoSeleccionado = data;      
        
      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
      }
    );
  }

 

  cargarEvaluaciones(): void {
    
    this.evaluacionService.listEvs(this.matricula).subscribe(
      data => {
        this.evaluaciones = data;
      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
      }
    );
  }

  


}
