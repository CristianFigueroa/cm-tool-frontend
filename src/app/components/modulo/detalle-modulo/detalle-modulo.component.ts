import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Alumno } from 'src/app/model/alumno';
import { Evaluacion } from 'src/app/model/evaluacion';
import { AlumnoService } from 'src/app/services/alumno.service';
import { EvaluacionService } from 'src/app/services/evaluacion.service';

@Component({
  selector: 'app-detalle-modulo',
  templateUrl: './detalle-modulo.component.html',
  styleUrls: ['./detalle-modulo.component.scss']
})
export class DetalleModuloComponent implements OnInit {

  @Input() modulo: Alumno[];
  evaluaciones: Evaluacion[];

  constructor(
    private alumnoService: AlumnoService,
    private toastr: ToastrService,
    private evaluacionService: EvaluacionService

  ) { }

  ngOnInit(): void {
    this.cargarAlumnos();
    this.cargarEvaluaciones()
  }


  cargarAlumnos() {
    this.alumnoService.list().subscribe(
      data => {
        this.modulo = data;
        this.cargarEvaluaciones()
        this.calcularPorcentajeModulo()
      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000

        });
      }
    )
  }
  cargarEvaluaciones(): void {
    this.evaluacionService.list().subscribe(
      data => {
        this.evaluaciones = data;
      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
      }
    );
  }

  calcularPorcentaje(evaluacion: Evaluacion): number {
    let cantidadAlumnos = this.modulo.length;
    let contador = 0;
    this.modulo.forEach(x => {
      if(x.rubricas)
      if (x.rubricas.find(y => y.evaluacion.numeroEvaluacion == evaluacion.numeroEvaluacion)) {
        contador++;
      }
    })
    if (cantidadAlumnos != 0) {
      return + ((contador * 100) / cantidadAlumnos).toFixed(0);
    }
    else
      return 0
  }

  calcularPorcentajeModulo() {
    if (this.evaluaciones) {
      let cantidadEvaluaciones = this.evaluaciones.length;
      let contador: number = 0;
      this.evaluaciones.forEach(x => {
        contador += this.calcularPorcentaje(x);
      })
      if (cantidadEvaluaciones > 0)
        return (contador / cantidadEvaluaciones).toFixed(0);

    }else
    return 100
  }
}
