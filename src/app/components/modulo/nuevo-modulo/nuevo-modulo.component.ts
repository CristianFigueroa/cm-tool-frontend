import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Modulo } from 'src/app/model/modulo';
import { Profesor } from 'src/app/model/profesor';
import { Semestre } from 'src/app/model/semestre';
import { ModuloService } from 'src/app/services/modulo.service';

@Component({
  selector: 'app-nuevo-modulo',
  templateUrl: './nuevo-modulo.component.html',
  styleUrls: ['./nuevo-modulo.component.scss']
})
export class NuevoModuloComponent implements OnInit {

  @Input() profesor: Profesor;
  @Output() moduloCreado = new EventEmitter<boolean>();

  codigoModulo: string;

  constructor(
    public moduloService: ModuloService,
    private toastr: ToastrService
  ) { }


  ngOnInit(): void {
  }

  crearModulo() {
    this.moduloService.new(new Modulo(this.codigoModulo), this.profesor.idProfesor).subscribe(
      data => {
        this.toastr.success(data.mensaje, "OK", {
          timeOut: 3000
        });
        this.moduloCreado.emit(true);

      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
      }
    );
  }





}
