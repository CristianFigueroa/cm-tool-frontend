import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Categoria } from 'src/app/model/categoria';
import { Evaluacion } from 'src/app/model/evaluacion';
import { Indicador } from 'src/app/model/indicador';
import { PlantillaService } from 'src/app/services/plantilla.service';
import { RubricaService } from 'src/app/services/rubrica.service';

@Component({
  selector: 'app-listar-categorias',
  templateUrl: './listar-categorias.component.html',
  styleUrls: ['./listar-categorias.component.scss'],
})
export class ListarCategoriasComponent implements OnInit {
  categorias: Categoria[] = [];
  indicadores: Indicador[];
  @Input() evaluacion: Evaluacion;
  actualizando: boolean = false;
  plantillaIndicadores = {};
  plantillaCategorias = {};

  constructor(
    private toastr: ToastrService,
    private rubricaService: RubricaService,
    private plantillaService: PlantillaService
  ) {}

  ngOnInit(): void {}

  ngOnChanges(_changes: any) {
    if (this.evaluacion) this.getPlantilla();
  }

  getPlantilla(): void {
    if (this.evaluacion) {
      if (!(this.evaluacion.numeroPlantilla in this.plantillaIndicadores))
        this.plantillaService
          .getByNivel(this.evaluacion.numeroPlantilla)
          .subscribe((data) => {
            this.indicadores = data;
            this.plantillaIndicadores[this.evaluacion.numeroPlantilla] =
              this.indicadores;
            console.log(this.indicadores);
            this.getCategorias();
          });
      else {
        this.indicadores =
          this.plantillaIndicadores[this.evaluacion.numeroPlantilla];
        this.categorias =
          this.plantillaCategorias[this.evaluacion.numeroPlantilla];
        this.getCategorias();
      }
    }
  }

  getCategorias() {
    this.categorias = [];
    if (this.indicadores)
      this.indicadores.forEach((x) => {
        if (
          !this.categorias.find((y) => y.idCategoria == x.categoria.idCategoria)
        ) {
          this.categorias.push(x.categoria);
        }
      });
    this.plantillaCategorias[this.evaluacion.idEvaluacion] = this.categorias;
  }
}
