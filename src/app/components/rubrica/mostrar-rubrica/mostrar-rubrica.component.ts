import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Rubrica } from 'src/app/model/rubrica';
import { RubricaService } from 'src/app/services/rubrica.service';
import { PlantillaService } from 'src/app/services/plantilla.service';
import { Alumno } from 'src/app/model/alumno';
import { Indicador } from 'src/app/model/indicador';
import { Categoria } from 'src/app/model/categoria';
import { Evaluacion } from 'src/app/model/evaluacion';

@Component({
  selector: 'app-mostrar-rubrica',
  templateUrl: './mostrar-rubrica.component.html',
  styleUrls: ['./mostrar-rubrica.component.scss']
})
export class MostrarRubricaComponent implements OnInit {

  @Input() alumno: Alumno;
  categorias: Categoria[] = [];
  indicadores: Indicador[];
  @Input() evaluacion: Evaluacion;
  actualizando: boolean = false;
  plantillaIndicadores = {};
  plantillaCategorias = {};
  loading = false;

  @Output() propagar = new EventEmitter<string>();

  constructor(
    private toastr: ToastrService,
    private rubricaService: RubricaService,
    private plantillaService: PlantillaService,
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(_changes: any) {
    if (this.evaluacion)
      this.getPlantilla();
  }

  getPlantilla(): void {
    if (this.evaluacion) {
      if (!(this.evaluacion.numeroPlantilla in this.plantillaIndicadores))
        this.plantillaService.getByNivel(this.evaluacion.numeroPlantilla).subscribe(
          data => {
            this.indicadores = data;
            this.plantillaIndicadores[this.evaluacion.numeroPlantilla] = this.indicadores;
            this.getCategorias();
          }
        )
      else {
        this.indicadores = this.plantillaIndicadores[this.evaluacion.numeroPlantilla];
        this.categorias = this.plantillaCategorias[this.evaluacion.numeroPlantilla];
        this.getCategorias();
      }
    }
    this.cargarPuntaje()
  }


  


  getCategorias() {
    this.categorias = []
    if (this.indicadores)
      this.indicadores.forEach(x => {
        if (!this.categorias.find(y => y.idCategoria == x.categoria.idCategoria)) {
          this.categorias.push(x.categoria)
        }
      })
      this.plantillaCategorias[this.evaluacion.idEvaluacion] = this.categorias;
  }

  cargarPuntaje(): void {
    if (this.alumno) {
      let rubrica = this.alumno.rubricas.find(x => x.evaluacion.idEvaluacion == this.evaluacion.idEvaluacion)
      if (rubrica) {
        if (this.alumno.rubricas.find(x => x.evaluacion.idEvaluacion == this.evaluacion.idEvaluacion))
          this.rubricaService.getByAlumno(this.evaluacion.idEvaluacion, this.alumno.matriculaAlumno).subscribe(
            data => {
              data.indicadores.forEach(x => {
                this.cambiarPuntaje(x.tipoIndicador.idTipoIndicador, x.puntajeIndicador, x.tipoIndicador.categoria.idCategoria)
              })
              this.actualizando = true;
            }
          )
      } else
        this.reiniciarPlantilla();
      this.actualizando = false;
    }
  }

  cambiarPuntaje(indicador: number, puntaje: number, categoria: number) {
    let j = document.getElementById("categoria " + categoria).getElementsByClassName("indicador " + indicador)[0];
    for (let i = 0; i < 5; i++) {
      let aux = i + "";
      if (aux == puntaje + "") {
        j.getElementsByClassName(aux)[0].setAttribute("style", "background-color: olive;")
        this.indicadores.map(x => {
          if (x.idTipoIndicador == indicador)
            x.puntajeIndicador = puntaje;
        })
      } else {
        j.getElementsByClassName(aux)[0].setAttribute("style", "background-color: darkslategray;")
      }
    }
  }

  reiniciarPlantilla() {
    let j = document.getElementsByClassName("indicador")
    for (let i = 0; i < j.length; i++) {
      for (let x = 0; x < 5; x++) {
        let aux = x+""
        if (j[i].getElementsByClassName(aux)[0])
        j[i].getElementsByClassName(aux)[0].setAttribute("style", "background-color: darkslategray;")
      }
    }

  }

}