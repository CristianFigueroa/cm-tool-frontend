import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { EvaluacionService } from 'src/app/services/evaluacion.service';
import { Evaluacion } from 'src/app/model/evaluacion';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-nueva-evaluacion',
  templateUrl: './nueva-evaluacion.component.html',
  styleUrls: ['./nueva-evaluacion.component.scss']
})
export class NuevaEvaluacionComponent implements OnInit { 

  evaluacionAux: Evaluacion;
  
  formularioNuevaEvaluacion: FormGroup;
  loading = false;

  constructor(
    private evaluacionService: EvaluacionService,
    private toastr: ToastrService,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.formularioNuevaEvaluacion = this.formBuilder.group({
      fecha: ['', Validators.required],
      numeroPlantilla: ['', Validators.required],
      descripcion: ['', Validators.required]
    });
    this.formularioNuevaEvaluacion.valueChanges.subscribe(val => {
      this.evaluacionAux = new Evaluacion(new Date(), val.numeroPlantilla, "");
    });
  }

  nuevo(evaluacion: Evaluacion): void {
    this.evaluacionService.create(evaluacion).subscribe(
      data => {
        this.toastr.success('Evaluacion creada', 'Ok', {
          timeOut: 3000
        });
        this.router.navigate(['/evaluaciones'])
      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
        this.loading = false;
      }
    )
  }

  submit() {
    this.loading = true;
    let fecha = this.formularioNuevaEvaluacion.get('fecha').value;
    let numeroPlantilla = this.formularioNuevaEvaluacion.get('numeroPlantilla').value;
    let descripcion = this.formularioNuevaEvaluacion.get('descripcion').value;
    this.nuevo(new Evaluacion(fecha, numeroPlantilla, descripcion));

  }
}
