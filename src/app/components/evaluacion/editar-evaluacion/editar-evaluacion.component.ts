import { Component, OnInit} from '@angular/core';
import { Evaluacion } from 'src/app/model/evaluacion';
import { EvaluacionService } from 'src/app/services/evaluacion.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-editar-evaluacion',
  templateUrl: './editar-evaluacion.component.html',
  styleUrls: ['./editar-evaluacion.component.scss']
})
export class EditarEvaluacionComponent implements OnInit {

  evaluacion: Evaluacion = null;

  formularioEditarEvaluacion: FormGroup;

  constructor(
    private evaluacionService: EvaluacionService,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.formularioEditarEvaluacion = this.formBuilder.group({
      fecha: ['', Validators.required],
      descripcion: ['', Validators.required]
    });
    const id = this.activatedRoute.snapshot.params.idEvaluacion;
    this.evaluacionService.getById(id).subscribe(
      data => {
        this.evaluacion = data;
        this.formularioEditarEvaluacion.patchValue({
          descripcion: this.evaluacion.descripcion,
          fecha: this.evaluacion.fechaEvaluacion
        }) 
      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
        this.router.navigate(['/evaluaciones'])

      }
    )

    
  }

  submit() {
    this.evaluacion.fechaEvaluacion = this.formularioEditarEvaluacion.get('fecha').value;
    this.evaluacion.descripcion = this.formularioEditarEvaluacion.get('descripcion').value;
    this.update();

  }

  update(): void {
    const id = this.activatedRoute.snapshot.params.idEvaluacion;
    this.evaluacionService.update(id, this.evaluacion).subscribe(
      data => {
        this.toastr.success('Evaluacion actualizada', 'Ok', {
          timeOut: 3000
        });
        this.evaluacion = data;
        this.router.navigate(['/evaluaciones'])
      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
        this.router.navigate(['/evaluaciones'])
      }
    )
  }

}
