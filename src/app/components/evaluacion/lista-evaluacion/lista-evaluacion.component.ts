import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TokenService } from 'src/app/services/token.service';
import { EvaluacionService } from 'src/app/services/evaluacion.service';
import { Evaluacion } from 'src/app/model/evaluacion';
import { Router } from '@angular/router';


@Component({
  selector: 'app-lista-evaluacion',
  templateUrl: './lista-evaluacion.component.html',
  styleUrls: ['./lista-evaluacion.component.scss']
})
export class ListaEvaluacionComponent implements OnInit {

  evaluaciones: Evaluacion[] = [];
  roles: string[];
  isAdmin = false;

  constructor(
    private evaluacionService: EvaluacionService,
    private toastr: ToastrService,
    private tokenService: TokenService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.cargarEvaluaciones();
    this.roles = this.tokenService.getAuthorities();
    this.roles.forEach(rol =>{
      if(rol === "ROLE_ADMIN"){
        this.isAdmin = true;
      }
    })    
  
  }

  cargarEvaluaciones(): void {
    this.evaluacionService.list().subscribe(
      data => {
        this.evaluaciones = data;
      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
        this.router.navigate(['/'])
      }
    );
  }

  eliminarEvaluacion(idEval: number){
    if(confirm("Esta acción es irreversible. ¿Estás seguro?")){
      this.evaluacionService.delete(idEval).subscribe(
        data => {
          this.toastr.success(data.mensaje, "OK", {
            timeOut: 3000
          });
          this.evaluaciones = this.evaluaciones.filter(evaluacion => evaluacion.idEvaluacion != idEval);
        }
      );
    }
  }


}
