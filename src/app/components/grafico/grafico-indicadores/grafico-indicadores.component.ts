import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  SimpleChanges,
} from '@angular/core';
import { Alumno } from 'src/app/model/alumno';
import { Categoria } from 'src/app/model/categoria';
import { Indicador } from 'src/app/model/indicador';
import { Rubrica } from 'src/app/model/rubrica';
import { PlantillaService } from 'src/app/services/plantilla.service';
import { Router } from '@angular/router';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-grafico-indicadores',
  templateUrl: './grafico-indicadores.component.html',
  styleUrls: ['./grafico-indicadores.component.scss'],
})
export class GraficoIndicadoresComponent implements OnInit {
  @Input() modulo: Alumno[];
  @Input() alumno: Alumno;
  @Input() hidden: boolean = true;
  @Output() regresar = new EventEmitter<string>();
  public categoriaSeleccionada: Categoria;
  public indicadores: Indicador[];

  public radarChartLabels = [];
  public radarChartData = [
    {
      label: '',
      data: [],
      hidden: false,
    },
  ];

  public radarChartOptions = {
    onClick: (event) => {
      let chartInstance = window.Chart.instances;
      let chart: any = chartInstance[Object.keys(chartInstance)[1]];
      let helpers = Chart.helpers;
      let eventPosition = helpers.getRelativePosition(event, chart);

      let mouseX = eventPosition.x;
      let mouseY = eventPosition.y;

      let activePoints = [];

      let scale = chart.scales.scale;
      let ctx = chart.ctx;
      helpers.each(
        scale.ticks,
        function (label, index) {
          for (var i = scale.pointLabels.length - 1; i >= 0; i--) {
            let pointLabelPosition = scale.getPointPosition(
              i,
              scale.getDistanceFromCenterForValue(
                scale.options.reverse ? scale.min : scale.max
              ) + 5
            );
            let pointLabelFontSize = helpers.getValueOrDefault(
              scale.options.pointLabels.fontSize,
              Chart.defaults.global.defaultFontSize
            );

            let labelsCount = scale.pointLabels.length,
              halfLabelsCount = scale.pointLabels.length / 2,
              quarterLabelsCount = halfLabelsCount / 2,
              upperHalf =
                i < quarterLabelsCount || i > labelsCount - quarterLabelsCount,
              exactQuarter =
                i === quarterLabelsCount ||
                i === labelsCount - quarterLabelsCount;
            let width = ctx.measureText(scale.pointLabels[i]).width / 3;
            let height = pointLabelFontSize * 5;

            let x;
            let y;
            if (i === 0 || i === halfLabelsCount)
              x = pointLabelPosition.x - width / 2;
            else if (i < halfLabelsCount) x = pointLabelPosition.x;
            else x = pointLabelPosition.x - width;

            if (exactQuarter) y = pointLabelPosition.y - height / 2;
            else if (upperHalf) y = pointLabelPosition.y - height;
            else y = pointLabelPosition.y;

            if (
              mouseY >= y &&
              mouseY <= y + height &&
              mouseX >= x &&
              mouseX <= x + width
            )
              activePoints.push({ index: i, label: scale.pointLabels[i] });
          }
        },
        scale
      );
      var firstPoint = activePoints[0];
      if (firstPoint != undefined) {
        if (this.alumno) {
          this.navegarAGraficoIndicador(firstPoint.label.toString());
        } else {
          this.regresar.emit(firstPoint.label.toString());
        }
      }
    },
    responsive: true,
    scale: {
      ticks: {
        beginAtZero: true,
        min: 0,
        max: 4,
        stepSize: 1,
      },
      pointLabels: {
        fontSize: 15,
      },
    },
    legend: {
      position: 'right',
      labels: {
        fontColor: 'black',
        fontSize: 15,
      },
    },
  };

  public radarChartType = 'radar';

  constructor(
    private plantillaService: PlantillaService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  ngOnChanges(_changeschanges: SimpleChanges) {
    if (this.categoriaSeleccionada) this.actualizarGrafico();
    //cambiar labels
  }

  navegarAGraficoIndicador(nombreIndicador: String) {
    if (this.alumno) {
      this.router.navigate([
        '/graficoindicador',
        nombreIndicador.toString().replace(/,/g, ' '),
        this.alumno.matriculaAlumno,
      ]);
    } else {
    }
  }

  identificarCategoria(callback?) {
    if (this.categoriaSeleccionada) {
      this.plantillaService
        .getByIdCategoria(this.categoriaSeleccionada.idCategoria)
        .subscribe((data) => {
          this.radarChartLabels = [];
          this.indicadores = data;
          this.indicadores.forEach((x) => {
            this.radarChartLabels.push(this.formatLabel(x.nombreIndicador, 40));
          });
          if (callback) callback();
        });
    }
  }

  actualizarGrafico() {
    this.identificarCategoria(() => {
      if (this.alumno) {
        this.cargarDatosAlumno(this.alumno);
        //let promedioAlumno = this.calcularPromedioAlumno(datosAlumno);
        //datosAlumno.unshift(promedioAlumno);
      } else {
        this.detalleModulo(this.modulo);
      }
    });
  }

  cargarDatosAlumno(alumno) {
    this.radarChartData = [];
    let index = 1;
    this.rubricasSort(alumno.rubricas).forEach((rubrica) => {
      let data = this.calcularDataRubrica(rubrica);
      let label = rubrica.evaluacion.descripcion;
      let hidden = true;
      if (
        alumno.rubricas.length == index ||
        alumno.rubricas.length - 1 == index
      ) {
        hidden = false;
      }
      this.radarChartData.push({ data, label, hidden });
      index++;
    });
  }

  calcularDataRubrica(rubrica: Rubrica): any[] {
    let data = [];
    if (this.indicadores)
      this.indicadores.forEach((indicador) => {
        let indicadorAux = rubrica.indicadores.find(
          (x) => x.tipoIndicador.idTipoIndicador == indicador.idTipoIndicador
        );
        if (indicadorAux) data.push(indicadorAux.puntajeIndicador);
        else data.push(0);
      });
    return data;
  }

  detalleModulo(alumnos: Alumno[]) {
    let radarChartData = [];
    let cantidadAlumnosXEval = new Map();

    alumnos.forEach((alumno) => {
      let index = 1;
      this.rubricasSort(alumno.rubricas).forEach((rubrica) => {
        if (isNaN(cantidadAlumnosXEval.get(rubrica.evaluacion.descripcion))) {
          cantidadAlumnosXEval.set(rubrica.evaluacion.descripcion, 1);
        } else
          cantidadAlumnosXEval.set(
            rubrica.evaluacion.descripcion,
            cantidadAlumnosXEval.get(rubrica.evaluacion.descripcion) + 1
          );
        let data = this.calcularDataRubrica(rubrica);
        let label = rubrica.evaluacion.descripcion;
        let hidden = true;
        if (
          alumno.rubricas.length == index ||
          alumno.rubricas.length - 1 == index
        ) {
          hidden = false;
        }
        index++;

        let evaluacionExiste: boolean = false;
        radarChartData.forEach((puntaje) => {
          if (puntaje.label == rubrica.evaluacion.descripcion) {
            evaluacionExiste = true;
            for (let i = 0; i < data.length; i++) {
              puntaje.data[i] += data[i];
            }
          }
        });
        if (!evaluacionExiste) {
          radarChartData.push({ data, label, hidden });
        }
      });
    });
    radarChartData = radarChartData.map(function (x) {
      x.data = x.data.map(function (y) {
        return y / cantidadAlumnosXEval.get(x.label);
      });
      return x;
    });
    this.radarChartData = radarChartData;
  }

  rubricasSort(rubricas: Rubrica[]): any[] {
    return rubricas.sort(function (a, b) {
      if (a.evaluacion.numeroEvaluacion > b.evaluacion.numeroEvaluacion) {
        return 1;
      }
      if (a.evaluacion.numeroEvaluacion < b.evaluacion.numeroEvaluacion) {
        return -1;
      }
      return 0;
    });
  }

  formatLabel(str, maxwidth) {
    var sections = [];
    var words = str.split(' ');
    var temp = '';

    words.forEach(function (item, index) {
      if (temp.length > 0) {
        var concat = temp + ' ' + item;
        if (concat.length > maxwidth) {
          sections.push(temp);
          temp = '';
        } else {
          if (index == words.length - 1) {
            sections.push(concat);
            return;
          } else {
            temp = concat;
            return;
          }
        }
      }

      if (index == words.length - 1) {
        sections.push(item);
        return;
      }

      if (item.length < maxwidth) {
        temp = item;
      } else {
        sections.push(item);
      }
    });

    return sections;
  }
}
