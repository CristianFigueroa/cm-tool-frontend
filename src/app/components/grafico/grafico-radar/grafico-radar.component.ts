import { Component, OnInit, Input, ViewChild, SimpleChanges } from '@angular/core';
import { Alumno } from 'src/app/model/alumno';
import { Categoria } from 'src/app/model/categoria';
import { Rubrica } from 'src/app/model/rubrica';
import { PlantillaService } from 'src/app/services/plantilla.service';
import { Chart } from 'chart.js';
import { GraficoIndicadoresComponent } from '../grafico-indicadores/grafico-indicadores.component';

@Component({
  selector: 'app-grafico-radar',
  templateUrl: './grafico-radar.component.html',
  styleUrls: ['./grafico-radar.component.scss']
})
export class GraficoRadarComponent implements OnInit {

  @ViewChild(GraficoIndicadoresComponent) graficoIndicadoresComponent: GraficoIndicadoresComponent;

  public categorias: Categoria[];

  @Input() public modulo: Alumno[];
  @Input() public alumno: Alumno;

  public hidden: boolean = false;
  
  constructor(
    private plantillaService: PlantillaService
  ) { }

  ngOnInit(): void {
    this.radarChartLabels = [];
    this.cargarCategorias();
    this.detalleModulo();
  }

  ngOnChanges(changes: SimpleChanges) {
    setTimeout(()=>{
      this.actualizarDatos()
    }) 
  }

  public radarChartLabels;
  public radarChartData = [{
    label: '',
    data: [],
    hidden: false,
  },
  ];

  public radarChartOptions = {
    onClick: (event) => {
      let chartInstance = window.Chart.instances
      let chart: any = chartInstance[Object.keys(chartInstance)[0]]
      let helpers = Chart.helpers;
      let eventPosition = helpers.getRelativePosition(event, chart);

      let mouseX = eventPosition.x;
      let mouseY = eventPosition.y;

      let activePoints = [];

      let scale = chart.scales.scale;
      let ctx = chart.ctx;
      helpers.each(scale.ticks, function (label, index) {
        for (var i = scale.pointLabels.length - 1; i >= 0; i--) {
          let pointLabelPosition = scale.getPointPosition(i, scale.getDistanceFromCenterForValue(scale.options.reverse ? scale.min : scale.max) + 5);
          let pointLabelFontSize = helpers.getValueOrDefault(scale.options.pointLabels.fontSize, Chart.defaults.global.defaultFontSize);

          let labelsCount = scale.pointLabels.length,
            halfLabelsCount = scale.pointLabels.length / 2,
            quarterLabelsCount = halfLabelsCount / 2,
            upperHalf = (i < quarterLabelsCount || i > labelsCount - quarterLabelsCount),
            exactQuarter = (i === quarterLabelsCount || i === labelsCount - quarterLabelsCount);
          let width = ctx.measureText(scale.pointLabels[i]).width / 3;
          let height = pointLabelFontSize * 5;

          let x;
          let y;
          if (i === 0 || i === halfLabelsCount)
            x = pointLabelPosition.x - width / 2;
          else if (i < halfLabelsCount)
            x = pointLabelPosition.x;
          else
            x = pointLabelPosition.x - width;

          if (exactQuarter)
            y = pointLabelPosition.y - height / 2;
          else if (upperHalf)
            y = pointLabelPosition.y - height;
          else
            y = pointLabelPosition.y

          if ((mouseY >= y && mouseY <= y + height) && (mouseX >= x && mouseX <= x + width))
            activePoints.push({ index: i, label: scale.pointLabels[i] });

        }
      }, scale)
      var firstPoint = activePoints[0];
      if (firstPoint != undefined) {
        this.cambiarGrafico(firstPoint.label.toString())
      }
    },
    responsive: true,
    scale: {
      ticks: {
        beginAtZero: true,
        min: 0,
        max: 4,
        stepSize: 1,
      },
      pointLabels: {
        fontSize: 15,
      }
    },
    legend: {
      position: 'right',
      labels: {
        fontColor: 'black',
        fontSize: 15,
      },
    }
  };

  public radarChartType = 'radar';

  cambiarGrafico(categoria?: string) {
    if (categoria) {
      this.graficoIndicadoresComponent.categoriaSeleccionada = this.categorias.find(x => {
        if (x.nombreCategoria.split(".")[0] == categoria.split('.')[0])
          return x;
      });
      this.graficoIndicadoresComponent.actualizarGrafico();
    }
    else
      this.graficoIndicadoresComponent.categoriaSeleccionada = null;
    this.hidden = !this.hidden;
    this.graficoIndicadoresComponent.hidden = !this.graficoIndicadoresComponent.hidden;
  }

  cargarCategorias() {
    this.plantillaService.getCategorias().subscribe(
      data => {
        this.categorias = data;
        this.categorias.forEach(x => {
          this.radarChartLabels.push(this.formatLabel(x.nombreCategoria, 40));
        });
      }
    )
  }

  formatLabel(str, maxwidth) {
    var sections = [];
    var words = str.split(" ");
    var temp = "";

    words.forEach(function (item, index) {
      if (temp.length > 0) {
        var concat = temp + ' ' + item;

        if (concat.length > maxwidth) {
          sections.push(temp);
          temp = "";
        }
        else {
          if (index == (words.length - 1)) {
            sections.push(concat);
            return;
          }
          else {
            temp = concat;
            return;
          }
        }
      }

      if (index == (words.length - 1)) {
        sections.push(item);
        return;
      }

      if (item.length < maxwidth) {
        temp = item;
      }
      else {
        sections.push(item);
      }

    });

    return sections;
  }

  actualizarDatos() {
    if (this.alumno) {
      this.cargarDatosAlumno(this.alumno);
    }
    else {
      this.detalleModulo();
    }
  }

  cargarDatosAlumno(alumno: Alumno) {
    this.radarChartData = [];
    let index = 1;
    if (alumno.rubricas)
      this.rubricasSort(alumno.rubricas).forEach(rubrica => {
        let data = this.calcularDataRubrica(rubrica);
        let label = rubrica.evaluacion.descripcion;
        let hidden = true;
        if (alumno.rubricas.length == index || alumno.rubricas.length - 1 == index) {
          hidden = false;
        }
        this.radarChartData.push({ data, label, hidden });
        index++;
      })
  }

  calcularPromedioAlumno(puntajesAlumno): any[] {
    let promedioChartData;
    let label = "Promedio Alumno";
    let hidden = false;
    let data = [];
    data = this.promedioDataPuntaje(puntajesAlumno);
    promedioChartData = { data, label, hidden };
    return promedioChartData;
  }

  calcularPromedioModulo(puntajesModulo): any[] {
    let promedioChartData;
    let label = "Promedio Modulo";
    let hidden = false;
    let data = [];
    data = this.promedioDataPuntaje(puntajesModulo);
    promedioChartData = { data, label, hidden };
    return promedioChartData;
  }

  promedioDataPuntaje(puntaje) {
    let data = [];
    if (puntaje.length > 0) {
      let puntajes = puntaje.reduce((a, b) => ({
        data: b.data.map((num, idx) => {
          return num + a.data[idx]
        })
      }));
      puntajes.data = puntajes.data.map(x => {
        return x / puntaje.length
      })
      data = puntajes.data
    }
    return data;
  }

  cargarDatosModulo(alumnos: Alumno[]): any[] {
    let radarChartData = [];
    let cantidadAlumnosXEval = new Map();
    if(alumnos)
    alumnos.forEach(alumno => {
      let index = 1;
      if (alumno.rubricas)
        this.rubricasSort(alumno.rubricas).forEach(rubrica => {
          if (isNaN(cantidadAlumnosXEval.get(rubrica.evaluacion.descripcion))) {
            cantidadAlumnosXEval.set(rubrica.evaluacion.descripcion, 1)
          }
          else
            cantidadAlumnosXEval.set(rubrica.evaluacion.descripcion, cantidadAlumnosXEval.get(rubrica.evaluacion.descripcion) + 1)
          let data = this.calcularDataRubrica(rubrica);
          let label = rubrica.evaluacion.descripcion;
          let hidden = true;
          if (alumno.rubricas.length == index || alumno.rubricas.length - 1 == index) {
            hidden = false;
          }
          index++;

          let evaluacionExiste: boolean = false;
          radarChartData.forEach(puntaje => {
            if (puntaje.label == rubrica.evaluacion.descripcion) {
              evaluacionExiste = true;
              for (let i = 0; i < data.length; i++) {
                puntaje.data[i] += data[i];
              }
            }
          })
          if (!evaluacionExiste) {
            radarChartData.push({ data, label, hidden })
          }
        })
    })
    radarChartData = radarChartData.map(function (x) {
      x.data = x.data.map(function (y) {
        return y / cantidadAlumnosXEval.get(x.label);
      })
      return x;
    })
    return radarChartData;
  }

  calcularDataRubrica(rubrica: Rubrica): any[] {
    let data = [];
    if (this.categorias)
      this.categorias.forEach(categoria => {
        let puntajeCategoria = 0;
        let contador = 0;
        rubrica.indicadores.forEach(indicador => {
          if (indicador.tipoIndicador.categoria.idCategoria == categoria.idCategoria) {
            contador++;
            puntajeCategoria += indicador.puntajeIndicador;
          }
        })
        let promedioCategoria = puntajeCategoria / contador;
        if (promedioCategoria)
          data.push(promedioCategoria);
        else
          data.push(0);
      })
    return data;
  }

  rubricasSort(rubricas: Rubrica[]): any[] {
    return rubricas.sort(function (a, b) {
      if (a.evaluacion.numeroEvaluacion > b.evaluacion.numeroEvaluacion) {
        return 1;
      }
      if (a.evaluacion.numeroEvaluacion < b.evaluacion.numeroEvaluacion) {
        return -1;
      }
      return 0;
    });
  }

  detalleModulo() {
    let dataModulo = this.cargarDatosModulo(this.modulo);
    this.radarChartData = dataModulo;
  }
}