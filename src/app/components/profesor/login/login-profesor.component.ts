import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';
import { LoginProfesorService } from 'src/app/services/login-profesor.service';
import { Router } from '@angular/router';
import { LoginProfesor } from 'src/app/model/login-profesor';

@Component({
  selector: 'app-login-profesor',
  templateUrl: './login-profesor.component.html',
  styleUrls: ['./login-profesor.component.scss'],
})
export class LoginProfesorComponent implements OnInit {
  isLogged: boolean;
  isLoginFail: boolean = false;
  loginProfesor: LoginProfesor;
  email: string;
  password: string;
  roles: string[] = [];
  loading = false;

  constructor(
    private tokenService: TokenService,
    private loginService: LoginProfesorService,
    private router: Router
  ) {}

  ngOnInit(): void {
    // si existe token es porque estamos logeados
    if (this.tokenService.getToken) {
      this.isLogged = false;
      this.isLoginFail = false;
      this.roles = this.tokenService.getAuthorities();
    }
  }

  loggin(): void {
    this.loading = true;
    this.loginProfesor = new LoginProfesor(this.email, this.password);
    this.loginService.login(this.loginProfesor).subscribe(
      (data) => {
        this.isLogged = true;
        this.isLoginFail = false;

        this.tokenService.setUsername(data.emailProfesor);
        this.tokenService.setToken(data.token);
        this.tokenService.setAuthorities(data.authorities);
        this.roles = data.authorities;
        this.router.navigate(['/index']);
      },
      (error) => {
        this.isLogged = false;
        this.isLoginFail = true;
        this.loading = false;
      }
    );
  }
}
