import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Profesor } from 'src/app/model/profesor';
import { ProfesorService } from 'src/app/services/profesor.service';

@Component({
  selector: 'app-nuevo-profesor',
  templateUrl: './nuevo-profesor.component.html',
  styleUrls: ['./nuevo-profesor.component.scss'],
})
export class NuevoProfesorComponent implements OnInit {
  @Output() profesorCreado = new EventEmitter<boolean>();

  emailPattern =
    '[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}';
  formularioNuevoProfesor: FormGroup;

  constructor(
    public profesorService: ProfesorService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.formularioNuevoProfesor = this.formBuilder.group({
      nombreProfesor: ['', Validators.required],
      correoProfesor: [
        '',
        [Validators.required, Validators.pattern(this.emailPattern)],
      ],
      contrasenias: this.formBuilder.group(
        {
          contraseniaProfesor: ['', Validators.required],
          contraseniaConfirm: ['', [Validators.required]],
        },
        { validator: this.passwordMatchValidator }
      ),
    });
  }

  crearProfesor(profesor: Profesor) {
    this.profesorService.new(profesor).subscribe(
      (data) => {
        this.toastr.success(data.mensaje, 'OK', {
          timeOut: 3000,
        });
        this.profesorCreado.emit(true);
      },
      (error) => {
        this.toastr.error(error.error.mensaje, 'Error', {
          timeOut: 3000,
        });
      }
    );
  }

  submit() {
    let nombreProfesor =
      this.formularioNuevoProfesor.get('nombreProfesor').value;
    let correoProfesor =
      this.formularioNuevoProfesor.get('correoProfesor').value;
    let contraseniaProfesor = this.formularioNuevoProfesor
      .get('contrasenias')
      .get('contraseniaProfesor').value;
    this.crearProfesor(
      new Profesor(nombreProfesor, correoProfesor, contraseniaProfesor)
    );
  }

  passwordMatchValidator(c: AbstractControl): { missMatch: boolean } {
    if (
      c.get('contraseniaProfesor').value !== c.get('contraseniaConfirm').value
    ) {
      return { missMatch: true };
    }
  }
}
