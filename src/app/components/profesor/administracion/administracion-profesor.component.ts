import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Alumno } from 'src/app/model/alumno';
import { Profesor } from 'src/app/model/profesor';
import { Semestre } from 'src/app/model/semestre';
import { AlumnoService } from 'src/app/services/alumno.service';
import { ProfesorService } from 'src/app/services/profesor.service';
import { SemestreService } from 'src/app/services/semestre.service';

@Component({
  selector: 'app-administracion-profesor',
  templateUrl: './administracion-profesor.component.html',
  styleUrls: ['./administracion-profesor.component.scss'],
})
export class AdministracionProfesorComponent implements OnInit {
  profesores: Profesor[];
  profesorSeleccionado: Profesor;
  modulo: Alumno[];
  semestre: Semestre;
  agregandoDocente: boolean = false;

  constructor(
    private toastr: ToastrService,
    private profesorService: ProfesorService,
    private alumnoService: AlumnoService,
    public semestreService: SemestreService
  ) {}

  ngOnInit(): void {
    this.cargarProfesores();
    this.cargarSemestre();
  }

  cargarProfesores(): void {
    this.profesorService.list().subscribe(
      (data) => {
        this.profesores = data;
        this.profesores = data.map(function (profesor) {
          profesor.moduloActivo = profesor.modulos.find(
            (modulo) => modulo.semestre.activo === true
          );
          return profesor;
        });
      },
      (error) => {
        this.toastr.error(error.error.mensaje, 'Error', {
          timeOut: 3000,
        });
      }
    );
    this.agregandoDocente = false;
  }

  cargarModulo(idProfesor: number): void {
    this.alumnoService.listAdmin(idProfesor).subscribe(
      (data) => {
        this.modulo = data;
      },
      (error) => {
        this.toastr.error(error.error.mensaje, 'Error', {
          timeOut: 3000,
        });
      }
    );
    this.agregandoDocente = false;
  }

  seleccionarProfesor(profesor: Profesor) {
    this.profesorSeleccionado = profesor;
    this.modulo = null;
    if (profesor) {
      this.cargarModulo(profesor.idProfesor);
    }
  }

  actualizarListaAlumnos() {
    this.cargarModulo(this.profesorSeleccionado.idProfesor);
  }

  cargarSemestre() {
    this.semestreService.getActivo().subscribe(
      (data) => {
        this.semestre = data;
      },
      (error) => {
        this.toastr.error(error.error.mensaje, 'Error', {
          timeOut: 3000,
        });
      }
    );
  }

  agregarDocente() {
    this.agregandoDocente = true;
  }
}
