import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { Profesor } from 'src/app/model/profesor';
import { ProfesorService } from 'src/app/services/profesor.service';
import { Router } from '@angular/router';
import { LoginProfesor } from 'src/app/model/login-profesor';
import { LoginProfesorService } from 'src/app/services/login-profesor.service';
import { TokenService } from 'src/app/services/token.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-listar-profesor',
  templateUrl: './listar-profesor.component.html',
  styleUrls: ['./listar-profesor.component.scss'],
})
export class ListarProfesorComponent implements OnInit, OnChanges {
  @Input() profesores: Profesor[];
  @Output() profesorSeleccionado = new EventEmitter<Profesor>();
  profesorSeleccionAnterior: Profesor;
  @Output() agregandoDocente = new EventEmitter<boolean>();
  profesoresParaCambiar: Profesor[] = [];
  seccionActual: string = '';
  isLogged: boolean;
  isLoginFail: boolean = false;
  loginProfesor: LoginProfesor;
  email: string;
  password: string;
  roles: string[] = [];
  loading = false;

  constructor(
    private toastr: ToastrService,
    private profesorService: ProfesorService,
    private loginService: LoginProfesorService,
    private tokenService: TokenService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  ngOnChanges() {
    this.seccionActual = '';
    if (this.profesorSeleccionAnterior)
      setTimeout(() => {
        document
          .getElementById('' + this.profesorSeleccionAnterior.idProfesor)
          .setAttribute('style', 'background-color: olive;');
        this.profesorSeleccionado.emit(
          this.profesores.find(
            (x) => x.idProfesor == this.profesorSeleccionAnterior.idProfesor
          )
        );
      });
  }

  enviarProfesor(profesorRecibido: Profesor): void {
    this.seccionActual = '';
    if (this.profesorSeleccionAnterior) {
      document
        .getElementById('' + this.profesorSeleccionAnterior.idProfesor)
        .setAttribute('style', '');
    }
    if (
      profesorRecibido &&
      this.profesorSeleccionAnterior != profesorRecibido
    ) {
      document
        .getElementById('' + profesorRecibido.idProfesor)
        .setAttribute('style', 'background-color: olive;');
      this.profesorSeleccionAnterior = profesorRecibido;
    } else {
      this.profesorSeleccionAnterior = null;
      profesorRecibido = null;
    }
    this.profesorSeleccionado.emit(profesorRecibido);
  }

  agregarProfesor(): void {
    this.seccionActual = 'agregarProfesor';
    this.enviarProfesor(null);
    this.agregandoDocente.emit(true);
  }

  cambiarProfesor(): void {
    this.seccionActual = 'cambiarProfesor';
    this.profesorService.list().subscribe((profesores) => {
      this.profesoresParaCambiar = profesores;
    });
  }

  loginById(profesorId: number, profesorNombre: string): void {
    console.log(profesorId);

    this.loading = true;
    this.loginService.loginById(profesorId).subscribe(
      (data) => {
        this.isLogged = true;
        this.isLoginFail = false;
        this.tokenService.setUsername(data.emailProfesor);
        this.tokenService.setToken(data.token);
        this.tokenService.setAuthorities(data.authorities);
        this.roles = data.authorities;
        this.toastr.success(
          `Se cambió correctamente al profesor ${profesorNombre}`
        );

        this.router.navigate(['/administracionDocente']);
      },
      (error) => {
        this.isLogged = false;
        this.isLoginFail = true;
        this.loading = false;
      }
    );
  }
}
