import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Semestre } from 'src/app/model/semestre';
import { SemestreService } from 'src/app/services/semestre.service';

@Component({
  selector: 'app-listar-semestre',
  templateUrl: './listar-semestre.component.html',
  styleUrls: ['./listar-semestre.component.scss']
})
export class ListarSemestreComponent implements OnInit {

  @Input() semestres: Semestre[];
  @Output() actualizarLista = new EventEmitter<boolean>();
  @Output() agregandoSemestre = new EventEmitter<boolean>();

  constructor(
    private semestreService: SemestreService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }

  activarSemestre(idSemestre: number) {
    if (confirm("Esta acción puede tener consecuencias en los módulos. ¿Estás seguro?")) {
      this.semestreService.cambiarActivo(idSemestre).subscribe(
        data => {
          this.toastr.success(data.mensaje, "OK", {
            timeOut: 3000
          });
          this.actualizarLista.emit(true);
        },
        error => {
          this.toastr.error(error.error.mensaje, "Error", {
            timeOut: 3000
          });
        }
      );
    }
  }

  agregarSemestre(){
    this.agregandoSemestre.emit(true)
  }

}
