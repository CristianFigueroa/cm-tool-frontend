import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Semestre } from 'src/app/model/semestre';
import { SemestreService } from 'src/app/services/semestre.service';

@Component({
  selector: 'app-nuevo-semestre',
  templateUrl: './nuevo-semestre.component.html',
  styleUrls: ['./nuevo-semestre.component.scss']
})
export class NuevoSemestreComponent implements OnInit {

  @Output() semestreCreado = new EventEmitter<boolean>();
  formularioNuevoSemestre: FormGroup;

  constructor(
    private semestreService: SemestreService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.formularioNuevoSemestre = this.formBuilder.group({
      anioSemestre: ['', [Validators.required, Validators.min(2020), Validators.max(3000)]],
      numeroSemestre: ['', [Validators.required, Validators.min(1), Validators.max(2)]]
    });
  }

  nuevoSemestre(semestre: Semestre) {
    this.semestreService.new(semestre).subscribe(
      data => {
        this.toastr.success(data.mensaje, "OK", {
          timeOut: 3000
        });
        this.semestreCreado.emit(true);
      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
      }
    );
  }

  submit(){
    let anioSemestre = this.formularioNuevoSemestre.get('anioSemestre').value;
    let numeroSemestre = this.formularioNuevoSemestre.get('numeroSemestre').value;
    this.nuevoSemestre(new Semestre(anioSemestre,numeroSemestre));
  }
}
