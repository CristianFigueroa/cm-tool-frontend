import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Semestre } from 'src/app/model/semestre';
import { SemestreService } from 'src/app/services/semestre.service';

@Component({
  selector: 'app-administracion-semestre',
  templateUrl: './administracion-semestre.component.html',
  styleUrls: ['./administracion-semestre.component.scss']
})
export class AdministracionSemestreComponent implements OnInit {

  semestres: Semestre[];
  agregandoSemestre: boolean = false;

  constructor(
    private toastr: ToastrService,
    private semestreService: SemestreService
  ) { }

  ngOnInit(): void {
    this.cargarSemestres();
  }

  cargarSemestres(): void {
    this.semestreService.list().subscribe(
      data => {
        this.semestres = data;
      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
      }
    );
    this.agregandoSemestre = false;
  }

  agregarSemestre(){
    this.agregandoSemestre = true;

  }

  

}
