import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Alumno } from 'src/app/model/alumno';
import { EvaluacionService } from 'src/app/services/evaluacion.service';
import { Evaluacion } from 'src/app/model/evaluacion';
import { AlumnoService } from 'src/app/services/alumno.service';

@Component({
  selector: 'app-detalle-alumno',
  templateUrl: './detalle-alumno.component.html',
  styleUrls: ['./detalle-alumno.component.scss']
})
export class DetalleAlumnoComponent implements OnInit {

  @Input() alumno: Alumno;
  @Input() evaluaciones: Evaluacion[];
  
  porcentaje: number;

  constructor(
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(_changes: any) {
    if (this.alumno)
      this.calcularPorcentaje()
  }

  alumnoEvaluado(alumno: Alumno, evaluacion: Evaluacion): boolean {
    if (alumno.rubricas)
      if (alumno.rubricas.find(x => x.evaluacion.idEvaluacion == evaluacion.idEvaluacion))
        return true;
    return false
  }

  calcularPorcentaje() {
    let cantidadEvaluaciones = this.evaluaciones.length;
    let contador = 0;

    this.evaluaciones.forEach(x => {
      if (this.alumnoEvaluado(this.alumno, x)) {
        contador++;
      }
    })
    if (cantidadEvaluaciones != 0) {
      let porcentajeNumber = + ((contador * 100) / cantidadEvaluaciones).toFixed(0)
      this.porcentaje = porcentajeNumber;
    }
    else
      this.porcentaje = 0;
  }




}
