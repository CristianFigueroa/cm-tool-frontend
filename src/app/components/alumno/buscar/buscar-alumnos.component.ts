import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';
import { LoginProfesorService } from 'src/app/services/login-profesor.service';
import { Router } from '@angular/router';
import { Alumno } from 'src/app/model/alumno';
import { ToastrService } from 'ngx-toastr';
import { Evaluacion } from 'src/app/model/evaluacion';
import { EvaluacionService } from 'src/app/services/evaluacion.service';
import { AlumnoService } from 'src/app/services/alumno.service';
import { LoginAlumno } from 'src/app/model/login-alumno';
import { LoginAlumnoService } from 'src/app/services/login-alumno.service';

@Component({
  selector: 'app-login-alumnos',
  templateUrl: './login-alumnos.component.html',
  styleUrls: ['./login-alumnos.component.scss']
})
export class LoginAlumnosComponent implements OnInit {

  isLogged: boolean;
  isLoginFail: boolean = false;
  loginAlumno: LoginAlumno;
  user: string;
  password: string;
  loading = false;
  alumnoSeleccionado: Alumno;
  roles: string[] = [];
  modulo: Alumno[]; 
  correct: boolean;
  matricula: string;
  matriculaexiste: boolean;

  constructor(
    private tokenService: TokenService,
    
    private router: Router,
    private toastr: ToastrService,
    private evaluacionService: EvaluacionService,
    private alumnoService: AlumnoService,
    private loginService: LoginAlumnoService,
  ) { }

  ngOnInit(): void {
    this.matriculaexiste=false;
  }

  buscamatricula():void{
    this.matriculaexiste=true;
    this.router.navigate(['/vista/',this.matricula])
    //buscar si existe la matricula si no existe mandar error
  }


}
