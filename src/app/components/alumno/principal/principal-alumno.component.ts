import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { Alumno } from 'src/app/model/alumno';
import { ToastrService } from 'ngx-toastr';
import { Evaluacion } from 'src/app/model/evaluacion';
import { EvaluacionService } from 'src/app/services/evaluacion.service';
import { AlumnoService } from 'src/app/services/alumno.service';

@Component({
  selector: 'app-principal-alumno',
  templateUrl: './principal-alumno.component.html',
  styleUrls: ['./principal-alumno.component.scss']
})
export class PrincipalAlumnoComponent implements OnInit {

  tipoEvaluacion: number = 0;

  alumnoSeleccionado: Alumno;
  @Output() modulo: Alumno[];
  evaluaciones: Evaluacion[];
  evaluacionSeleccionada: Evaluacion;

  constructor(
    private toastr: ToastrService,
    private evaluacionService: EvaluacionService,
    private alumnoService: AlumnoService
  ) { }

  ngOnInit(): void {
    this.cargarAlumnos()
    this.cargarEvaluaciones();
    
  }

  seleccionarAlumno(alumno: Alumno): void {
    if (this.alumnoSeleccionado === alumno) {
      this.alumnoSeleccionado = null;
    } else {
      this.alumnoSeleccionado = alumno;
    }
  }

  cambiarEvaluacion(tipoEvaluacion: number, evaluacion: any) {
    this.tipoEvaluacion = tipoEvaluacion;
    this.evaluacionSeleccionada = evaluacion;
    if (this.tipoEvaluacion == 0) {
      if (this.alumnoSeleccionado) {    // no eval, si alum
        this.evaluacionSeleccionada = null;
      } else {                          // no eval, no alum
        this.alumnoSeleccionado = null;
      }
    }
    if (this.tipoEvaluacion != 0) {
      if (this.alumnoSeleccionado) {    // si eval, si alum 

      } else {                          // si eval, no alum
        this.alumnoSeleccionado = null;
      }
    }
  }

  cargarAlumnos(): void {
    this.alumnoService.list().subscribe(
      data => {
        this.modulo = data;
      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
      }
    );
  }

  cargarEvaluaciones(): void {
    this.evaluacionService.list().subscribe(
      data => {
        this.evaluaciones = data;
      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
      }
    );
  }

  actualizarLista(){
    this.cambiarEvaluacion(0,0);
    this.cargarAlumnos();
  }

  
}
