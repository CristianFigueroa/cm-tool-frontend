import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import {
  FileSystemDirectoryEntry,
  FileSystemFileEntry,
  NgxFileDropEntry,
} from 'ngx-file-drop';
import { ToastrService } from 'ngx-toastr';
import { Profesor } from 'src/app/model/profesor';
import { AlumnoService } from 'src/app/services/alumno.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-cargar-alumno',
  templateUrl: './cargar-alumno.component.html',
  styleUrls: ['./cargar-alumno.component.scss'],
})
export class CargarAlumnoComponent implements OnInit {
  public files: NgxFileDropEntry[] = [];
  file: File;
  roles: string[];
  isAdmin = false;
  @Output() propagar = new EventEmitter<boolean>(); //señala al componente padre que se cargaron los alumnos
  @Input() profesor: Profesor;

  constructor(
    private alumnoService: AlumnoService,
    private toastr: ToastrService,
    private tokenService: TokenService
  ) {}

  ngOnInit(): void {
    this.roles = this.tokenService.getAuthorities();
    this.roles.forEach((rol) => {
      if (rol === 'ROLE_ADMINISTRADOR') {
        this.isAdmin = true;
      }
    });
  }

  public dropped(files: NgxFileDropEntry[]) {
    let idProfesor = this.profesor.idProfesor;
    this.files = files;
    for (const droppedFile of files) {
      if (
        droppedFile.fileEntry.isFile &&
        this.isFileAllowed(droppedFile.fileEntry.name)
      ) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.alumnoService.nuevos(file, idProfesor).subscribe(
            (data) => {
              this.toastr.success('Estudiantes cargados', 'Ok', {
                timeOut: 3000,
              });
              this.propagar.emit(true);
            },
            (error) => {
              this.toastr.error(error.error.mensaje, 'Error', {
                timeOut: 3000,
              });
            }
          );
        });
      } else {
        this.toastr.error('La extension del archivo debe ser .xls', 'Error', {
          timeOut: 3000,
        });
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      }
    }
  }

  public fileOver(event) {}

  public fileLeave(event) {}

  isFileAllowed(fileName: string) {
    let isFileAllowed = false;
    const allowedFiles = ['.xls'];
    const regex = /(?:\.([^.]+))?$/;
    const extension = regex.exec(fileName);
    if (undefined !== extension && null !== extension) {
      for (const ext of allowedFiles) {
        if (ext === extension[0]) {
          isFileAllowed = true;
        }
      }
    }
    return isFileAllowed;
  }
}
