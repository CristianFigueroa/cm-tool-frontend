import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges,
} from '@angular/core';
import { Alumno } from 'src/app/model/alumno';
import { Profesor } from 'src/app/model/profesor';
import { TokenService } from 'src/app/services/token.service';
import { AlumnoService } from 'src/app/services/alumno.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-lista-alumno',
  templateUrl: './lista-alumno.component.html',
  styleUrls: ['./lista-alumno.component.scss'],
})
export class ListaAlumnoComponent implements OnInit, OnChanges {
  @Input() profesor: Profesor;
  @Input() modulo: Alumno[];
  alumnoSeleccionAnterior: Alumno;
  @Output() alumnoSeleccionado = new EventEmitter<Alumno>();
  @Output() actualizarAlumnos = new EventEmitter<boolean>();
  filterNombre: string = '';
  @Input() mostrarBotonEliminar: boolean;

  idEvaluacion: number = null;
  isAdmin = false;
  matriculaAlumno: string;
  nombreAlumno: string;
  constructor(
    private tokenService: TokenService,
    private alumnoService: AlumnoService
  ) {}

  ngOnInit(): void {
    let roles = this.tokenService.getAuthorities();
    roles.forEach((rol) => {
      if (rol === 'ROLE_ADMINISTRADOR') {
        this.isAdmin = true;
      }
    });
    this.modulo = this.modulo.map((alumno) => ({ ...alumno, editando: false }));
  }

  ngOnChanges() {
    if (this.alumnoSeleccionAnterior)
      setTimeout(() => {
        document
          .getElementById('' + this.alumnoSeleccionAnterior.matriculaAlumno)
          .setAttribute('style', 'background-color: olive;');
        this.alumnoSeleccionado.emit(
          this.modulo.find(
            (x) =>
              x.matriculaAlumno == this.alumnoSeleccionAnterior.matriculaAlumno
          )
        );
      });
  }

  enviarAlumno(alumnoRecibido: Alumno): void {
    if (
      this.alumnoSeleccionAnterior &&
      document.getElementById(this.alumnoSeleccionAnterior.matriculaAlumno)
    ) {
      document
        .getElementById(this.alumnoSeleccionAnterior.matriculaAlumno)
        .setAttribute('style', '');
    }
    if (this.alumnoSeleccionAnterior != alumnoRecibido) {
      document
        .getElementById(alumnoRecibido.matriculaAlumno)
        .setAttribute('style', 'background-color: olive;');
      this.alumnoSeleccionAnterior = alumnoRecibido;
    } else {
      this.alumnoSeleccionAnterior = null;
      alumnoRecibido = null;
    }
    this.alumnoSeleccionado.emit(alumnoRecibido);
  }

  actualizarLista() {
    this.actualizarAlumnos.emit(true);
  }

  eliminarAlumno(alumno: Alumno): void {
    Swal.fire({
      title: '¿Estás seguro?',
      text: `¿Seguro que quieres eliminar al alumno matrícula: ${alumno.matriculaAlumno}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'No, cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.alumnoService.delete(alumno.matriculaAlumno).subscribe(
          () => {
            this.actualizarLista();
          },
          (error) => {
            console.error('Error eliminando alumno:', error);
          }
        );
      }
    });
  }

  actualizarAlumno(matricula: string, nuevoNombre: string) {
    this.alumnoService.actualizarAlumno(matricula, nuevoNombre).subscribe(
      (response) => {
        this.actualizarLista();
        console.log(response);
      },
      (error) => {
        // Aquí puedes manejar los errores
        console.error('Error:', error);
      }
    );
  }

  agregarAlumno() {
    this.alumnoService
      .agregarAlumno(
        this.profesor.idProfesor,
        this.nombreAlumno,
        this.matriculaAlumno
      )
      .subscribe(
        (response) => {
          // Aquí puedes manejar la respuesta del servidor
          console.log(response);
          this.matriculaAlumno = '';
          this.nombreAlumno = '';
          // Y finalmente, puedes actualizar la lista de alumnos
          this.actualizarLista();
        },
        (error) => {
          // Aquí puedes manejar los errores
          console.error(error);
        }
      );
  }
}
