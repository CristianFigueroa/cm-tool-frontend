import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Evaluacion } from 'src/app/model/evaluacion';
import { EvaluacionService } from 'src/app/services/evaluacion.service';
import { AlumnoService } from 'src/app/services/alumno.service';
import { LoginAlumnoService } from 'src/app/services/login-alumno.service';


@Component({
  selector: 'app-login-alumnos',
  templateUrl: './login-alumnos.component.html',
  styleUrls: ['./login-alumnos.component.scss']
})
export class LoginAlumnosComponent implements OnInit {

  
  isLoginFail: boolean = false;
  matricula: string;
  loading = false;
  correct: boolean;
  evaluaciones: Evaluacion[];

  constructor(
    private tokenService: TokenService,
    
    private router: Router,
    private toastr: ToastrService,
    private evaluacionService: EvaluacionService,
    private alumnoService: AlumnoService,
    private loginService: LoginAlumnoService,
  ) { }

  ngOnInit(): void {
    
  }


  loggin():void{
    this.loading = true;

    this.alumnoService.getById(this.matricula).subscribe(
      data => {
        
        this.isLoginFail = false;
        //cambiar redirect
        this.router.navigate(['/vista/',this.matricula])
      },
      error =>{
        
        this.isLoginFail = true;
        this.loading = false;
      }
    )
  }

  cargarEvaluaciones(): void {
    
    this.evaluacionService.listEvs(this.matricula).subscribe(
      data => {
        this.evaluaciones = data;
      },
      error => {
        this.toastr.error(error.error.mensaje, "Error", {
          timeOut: 3000
        });
      }
    );
  }


}
