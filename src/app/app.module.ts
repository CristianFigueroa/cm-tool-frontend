import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginProfesorComponent } from './components/profesor/login/login-profesor.component';
import { MenuComponent } from './components/menu/menu/menu.component';
import { IndexComponent } from './components/index/index/index.component';
import { interceptorProvider } from './services/interceptor.service';
import { NavegacionComponent } from './components/emra2/barra-navegacion-inferior/barra-navegacion-inferior.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxFileDropModule } from 'ngx-file-drop';
import { ListaAlumnoComponent } from './components/alumno/listar/lista-alumno.component';
import { DetalleAlumnoComponent } from './components/alumno/detalle/detalle-alumno.component';
import { CargarAlumnosComponent } from './components/modulo/cargar-alumnos/cargar-alumnos.component';
import { ListaEvaluacionComponent } from './components/evaluacion/lista-evaluacion/lista-evaluacion.component';
import { NuevaEvaluacionComponent } from './components/evaluacion/nueva-evaluacion/nueva-evaluacion.component';
import { EditarEvaluacionComponent } from './components/evaluacion/editar-evaluacion/editar-evaluacion.component';
import { ListarRubricaComponent } from './components/rubrica/listar-rubrica/listar-rubrica.component';
import { EditarRubricaComponent } from './components/rubrica/editar-rubrica/editar-rubrica.component';
import { EvaluadosPipe } from './pipes/evaluados.pipe';
import { CargarAlumnoComponent } from './components/alumno/cargar/cargar-alumno/cargar-alumno.component';
import { GraficoRadarComponent } from './components/grafico/grafico-radar/grafico-radar.component';
import { DetalleModuloComponent } from './components/modulo/detalle-modulo/detalle-modulo.component';
import { PrincipalAlumnoComponent } from './components/alumno/principal/principal-alumno.component';
import { GraficoIndicadoresComponent } from './components/grafico/grafico-indicadores/grafico-indicadores.component';
import { AdministracionProfesorComponent } from './components/profesor/administracion/administracion-profesor.component';
import { ListarProfesorComponent } from './components/profesor/listar/listar-profesor.component';
import { EditarProfesorComponent } from './components/profesor/editar/editar-profesor.component';
import { NuevoModuloComponent } from './components/modulo/nuevo-modulo/nuevo-modulo.component';
import { ListarSemestreComponent } from './components/semestre/listar/listar-semestre.component';
import { NuevoProfesorComponent } from './components/profesor/nuevo/nuevo-profesor.component';
import { AdministracionSemestreComponent } from './components/semestre/administracion/administracion-semestre.component';
import { NuevoSemestreComponent } from './components/semestre/nuevo/nuevo-semestre.component';
import { FiltroNombreAlumnoPipe } from './pipes/filtro-nombre-alumno.pipe';
import { ListarCategoriasComponent } from './components/rubrica/listar-categorias/listar-categorias.component';
import { VistaAluComponent } from './components/vista-alu/vista-alu.component';
import { LoginAlumnosComponent } from './components/login-alumnos/login-alumnos.component';
import { MostrarRubricaComponent } from './components/rubrica/mostrar-rubrica/mostrar-rubrica.component';

import { ResumenEMRA2Component } from './components/emra2/resumen/resumen-emra2.component';
import { GraficoIndicadorComponent } from './components/emra2/grafico-indicador/grafico-indicador.component';
import { ResumenIndicadoresComponent } from './components/emra2/resumen-indicadores/resumen-indicadores.component';
import { ResumenRasComponent } from './components/emra2/resumen-ras/resumen-ras.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginProfesorComponent,
    MenuComponent,
    NavegacionComponent,
    IndexComponent,
    ListaAlumnoComponent,
    DetalleAlumnoComponent,
    CargarAlumnosComponent,
    ListaEvaluacionComponent,
    NuevaEvaluacionComponent,
    EditarEvaluacionComponent,
    ListarRubricaComponent,
    EditarRubricaComponent,
    EvaluadosPipe,
    CargarAlumnoComponent,
    GraficoRadarComponent,
    DetalleModuloComponent,
    PrincipalAlumnoComponent,
    GraficoIndicadoresComponent,
    AdministracionProfesorComponent,
    ListarProfesorComponent,
    EditarProfesorComponent,
    NuevoModuloComponent,
    ListarSemestreComponent,
    NuevoProfesorComponent,
    AdministracionSemestreComponent,
    NuevoSemestreComponent,
    FiltroNombreAlumnoPipe,
    ListarCategoriasComponent,
    VistaAluComponent,
    LoginAlumnosComponent,
    MostrarRubricaComponent,

    ResumenEMRA2Component,
    GraficoIndicadorComponent,
    ResumenIndicadoresComponent,
    ResumenRasComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxFileDropModule,
    ChartsModule,
    ReactiveFormsModule,
  ],
  providers: [interceptorProvider],
  bootstrap: [AppComponent],
})
export class AppModule {}
