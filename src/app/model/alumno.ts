import { AlumnoService } from '../services/alumno.service';
import { Rubrica } from './rubrica';

export class Alumno {
    alumnoService:AlumnoService;

    matriculaAlumno: string;
    nombreAlumno: string;
    codigoCarrera: number;
    notas: [];
    rubricas: Rubrica[];
    evaluado?: boolean;


    constructor(matricula: string, nombre: string, codigoCarrera: number) {
        this.matriculaAlumno = matricula;
        this.nombreAlumno = nombre;
        this.codigoCarrera = codigoCarrera;
    }

}
