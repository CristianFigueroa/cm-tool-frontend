export class Semestre {

    idSemestre: number
    anioSemestre: number;
    numeroSemestre: number;
    activo: boolean;

    constructor(anio:number, numero:number){
        this.anioSemestre = anio;
        this.numeroSemestre = numero;
    }
}