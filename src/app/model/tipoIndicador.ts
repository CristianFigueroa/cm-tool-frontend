import { Categoria } from './categoria';

export class TipoIndicador {
    idTipoIndicador?: number;
    nombreIndicador: string
    categoria?: Categoria;
    criterio0: string;
    criterio1: string;
    criterio2: string;
    criterio3: string;
    criterio4: string
    semanas: number[];

}
