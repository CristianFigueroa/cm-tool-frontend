import { Categoria } from './categoria';
import { TipoIndicador } from './tipoIndicador';



export class Indicador {
    puntajeIndicador: number;
    idTipoIndicador?: number;
    nombreIndicador?: string
    categoria?: Categoria;
    tipoIndicador?: TipoIndicador;
    

    setPuntaje(puntaje: number):void{
        this.puntajeIndicador = puntaje;
    }
}
