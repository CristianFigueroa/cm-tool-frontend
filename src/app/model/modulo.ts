import { Profesor } from "./profesor";
import { Semestre } from "./semestre";

export class Modulo {
    codigoModulo: string;
    semestre: Semestre;
    profesor: Profesor;

    constructor(codigoModulo: string) {
        this.codigoModulo = codigoModulo;
    }
}
