import { Modulo } from "./modulo";

export class Profesor {
    idProfesor?: number; // :? quiere decir que no es un dato obligatorio
    nombreProfesor: string;
    correoProfesor: string;
    contraseniaProfesor: string;
    rol: string[];

    modulos: Modulo[];

    moduloActivo: Modulo;

    constructor(nombreProfesor: string, correoProfesor: string, contraseniaProfesor: string) {
        this.nombreProfesor = nombreProfesor;
        this.correoProfesor = correoProfesor;
        this.contraseniaProfesor = contraseniaProfesor;
    }
}
