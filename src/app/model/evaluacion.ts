export class Evaluacion {
    idEvaluacion?: number;
    fechaEvaluacion: Date;
    numeroEvaluacion: number;
    descripcion: String;
    numeroPlantilla: number;
    idProfesor: number;

    constructor(fechaEvaluacion: Date, numeroPlantilla: number, descripcion: String) {
        this.fechaEvaluacion = fechaEvaluacion;
        this.numeroPlantilla = numeroPlantilla;
        this.descripcion = descripcion;
    }
}
