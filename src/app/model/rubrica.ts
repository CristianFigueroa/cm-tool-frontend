import { Evaluacion } from './evaluacion';
import { Indicador } from './indicador';

export class Rubrica {
    id?: number; // :? quiere decir que no es un dato obligatorio
    indicadores: Indicador[];
    evaluacion:Evaluacion;

    constructor(indicadores: Indicador[]) {
        this.indicadores = indicadores;

    }
}
