import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Evaluacion } from 'src/app/model/evaluacion';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EvaluacionService {
  env = environment;
  evaluacionUrl = this.env.urlBackendCMTOOL + 'evaluacion/';

  constructor(private httpClient: HttpClient) {}

  public listEvs(matricula: string): Observable<Evaluacion[]> {
    return this.httpClient.get<Evaluacion[]>(
      this.evaluacionUrl + `lista/${matricula}`
    );
  }
  public list(): Observable<Evaluacion[]> {
    return this.httpClient.get<Evaluacion[]>(this.evaluacionUrl + 'lista');
  }
  public lista(mail: string): Observable<Evaluacion[]> {
    return this.httpClient.get<Evaluacion[]>(
      this.evaluacionUrl + `lista/${mail}`
    );
  }

  public getById(id: number): Observable<Evaluacion> {
    return this.httpClient.get<Evaluacion>(
      this.evaluacionUrl + `detalle/${id}`
    );
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.evaluacionUrl + `eliminar/${id}`);
  }

  public create(evaluacion: Evaluacion): Observable<any> {
    return this.httpClient.post<any>(this.evaluacionUrl + `nuevo`, evaluacion);
  }

  public update(id: number, evaluacion: Evaluacion): Observable<any> {
    return this.httpClient.put<any>(
      this.evaluacionUrl + `actualizar/${id}`,
      evaluacion
    );
  }

  public findEvaluacionPuntajePorIndicadorMatricula(
    nombreIndicador: string,
    matricula: string
  ): Observable<any> {
    return this.httpClient.get<any>(
      `${this.evaluacionUrl}puntaje/${matricula}/${nombreIndicador}`
    );
  }

  public findEvaluacionPorDesc(desc: String): Observable<any> {
    return this.httpClient.get<any>(`${this.evaluacionUrl}descripcion/${desc}`);
  }
}
