import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Semestre } from '../model/semestre';

@Injectable({
  providedIn: 'root',
})
export class SemestreService {
  env = environment;
  semestreUrl = this.env.urlBackendCMTOOL + 'semestre/';

  constructor(private httpClient: HttpClient) {}

  public list(): Observable<Semestre[]> {
    return this.httpClient.get<Semestre[]>(this.semestreUrl + 'lista');
  }

  public getActivo(): Observable<Semestre> {
    return this.httpClient.get<Semestre>(this.semestreUrl + `activo`);
  }

  public new(semestre: Semestre): Observable<any> {
    return this.httpClient.post<any>(this.semestreUrl + `nuevo`, semestre);
  }

  public cambiarActivo(id: number): Observable<any> {
    return this.httpClient.put<any>(
      this.semestreUrl + `cambiarActivo/${id}`,
      null
    );
  }
}
