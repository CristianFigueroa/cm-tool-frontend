import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Modulo } from '../model/modulo';

@Injectable({
  providedIn: 'root',
})
export class ModuloService {
  env = environment;
  moduloUrl = this.env.urlBackendCMTOOL + 'modulo/';

  constructor(private httpClient: HttpClient) {}

  public list(): Observable<Modulo[]> {
    return this.httpClient.get<Modulo[]>(this.moduloUrl + 'lista');
  }

  public getByCodigoModulo(codigoModulo: string): Observable<Modulo> {
    return this.httpClient.get<Modulo>(
      this.moduloUrl + `detalle/${codigoModulo}`
    );
  }

  public getModulo(): Observable<Modulo> {
    return this.httpClient.get<Modulo>(this.moduloUrl + `detalle`);
  }

  public new(modulo: Modulo, idProfesor: number): Observable<any> {
    return this.httpClient.post<any>(
      this.moduloUrl + `nuevo/${idProfesor}`,
      modulo
    );
  }
}
