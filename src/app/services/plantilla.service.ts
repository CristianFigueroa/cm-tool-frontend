import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Indicador } from 'src/app/model/indicador';
import { Categoria } from '../model/categoria';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PlantillaService {
  env = environment;
  plantillaUrl = this.env.urlBackendCMTOOL + 'plantilla/';

  constructor(private httpClient: HttpClient) {}

  public getByNivel(nivel: number): Observable<Indicador[]> {
    return this.httpClient.get<Indicador[]>(
      this.plantillaUrl + `listar/${nivel}`
    );
  }

  public getByIdCategoria(idCategioria: number): Observable<Indicador[]> {
    return this.httpClient.get<Indicador[]>(
      this.plantillaUrl + `listarByIdCategoria/${idCategioria}`
    );
  }

  public getCategorias(): Observable<Categoria[]> {
    return this.httpClient.get<Categoria[]>(
      this.plantillaUrl + `listar/categoria`
    );
  }

  public getNombreIndicadores(): Observable<String[]> {
    return this.httpClient.get<String[]>(
      this.plantillaUrl + 'listar/nombresIndicadores'
    );
  }
}
