import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Profesor } from 'src/app/model/profesor';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ProfesorService {
  env = environment;
  profesorUrl = this.env.urlBackendCMTOOL + 'profesor/';

  constructor(private httpClient: HttpClient) {}

  public list(): Observable<Profesor[]> {
    return this.httpClient.get<Profesor[]>(this.profesorUrl + 'lista');
  }

  public getById(id: number): Observable<Profesor> {
    return this.httpClient.get<Profesor>(this.profesorUrl + `detalle/${id}`);
  }

  public new(profesor: Profesor): Observable<any> {
    return this.httpClient.post<any>(this.profesorUrl + `nuevo`, profesor);
  }

  public update(id: number, profesor: Profesor): Observable<any> {
    return this.httpClient.put<any>(
      this.profesorUrl + `editar/${id}`,
      profesor
    );
  }

  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.profesorUrl + `eliminar/${id}`);
  }
}
