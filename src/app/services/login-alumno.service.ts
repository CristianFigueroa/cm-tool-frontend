import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { LoginAlumno } from 'src/app/model/login-alumno';
import { JwtDto } from '../model/jwt-dto';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LoginAlumnoService {
  env = environment;
  authURL = this.env.urlBackendCMTOOL + 'autentificacion/';

  constructor(private httpClient: HttpClient) {}

  public login(loginAlumno: LoginAlumno): Observable<JwtDto> {
    return this.httpClient.post<JwtDto>(this.authURL + `loginb`, loginAlumno);
  }
}
