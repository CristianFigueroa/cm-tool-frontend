import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Rubrica } from 'src/app/model/rubrica';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RubricaService {
  env = environment;
  rubricaUrl = this.env.urlBackendCMTOOL + 'rubrica/';

  constructor(private httpClient: HttpClient) {}

  public list(): Observable<Rubrica[]> {
    return this.httpClient.get<Rubrica[]>(this.rubricaUrl + 'lista');
  }

  public getById(id: number): Observable<Rubrica> {
    return this.httpClient.get<Rubrica>(this.rubricaUrl + `detalle/${id}`);
  }

  public new(
    idEvaluacion: number,
    matricula: string,
    rubrica: Rubrica
  ): Observable<any> {
    return this.httpClient.post<any>(
      this.rubricaUrl + `nuevo/${idEvaluacion}/${matricula}`,
      rubrica
    );
  }

  public update(
    idEvaluacion: number,
    matricula: string,
    rubrica: Rubrica
  ): Observable<any> {
    return this.httpClient.put<any>(
      this.rubricaUrl + `editar/${idEvaluacion}/${matricula}`,
      rubrica
    );
  }

  public getByAlumno(
    idEvaluacion: number,
    matricula: string
  ): Observable<Rubrica> {
    return this.httpClient.get<Rubrica>(
      this.rubricaUrl + `${idEvaluacion}/${matricula}`
    );
  }
}
