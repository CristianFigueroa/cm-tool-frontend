import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginProfesor } from 'src/app/model/login-profesor';

import { JwtDto } from '../model/jwt-dto';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LoginProfesorService {
  env = environment;
  authURL = this.env.urlBackendCMTOOL + 'autentificacion/';

  constructor(private httpClient: HttpClient) {}

  public login(loginProfesor: LoginProfesor): Observable<JwtDto> {
    return this.httpClient.post<JwtDto>(this.authURL + `login`, loginProfesor);
  }

  public loginById(profesorId: number): Observable<JwtDto> {
    return this.httpClient.post<JwtDto>(this.authURL + `loginById`, {
      profesorId: profesorId,
    });
  }
}
