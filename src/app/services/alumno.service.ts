import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Alumno } from 'src/app/model/alumno';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AlumnoService {
  env = environment;
  alumnoUrl = this.env.urlBackendCMTOOL + 'alumno/';

  constructor(private httpClient: HttpClient) {}

  public list(): Observable<Alumno[]> {
    return this.httpClient.get<Alumno[]>(this.alumnoUrl + `lista`);
  }

  public listAdmin(idProfesor: number): Observable<Alumno[]> {
    return this.httpClient.get<Alumno[]>(
      this.alumnoUrl + `listaAdmin/${idProfesor}`
    );
  }

  public getById(matricula: string): Observable<Alumno> {
    return this.httpClient.get<Alumno>(this.alumnoUrl + `detalle/${matricula}`);
  }

  public getDatosById(matricula: string): Observable<Alumno> {
    return this.httpClient.get<Alumno>(this.alumnoUrl + `datos/${matricula}`);
  }
  public getModuloById(matricula: string): Observable<Alumno> {
    return this.httpClient.get<Alumno>(this.alumnoUrl + `modulo/${matricula}`);
  }

  public new(alumno: Alumno): Observable<any> {
    return this.httpClient.post<any>(this.alumnoUrl + `nuevo`, alumno);
  }

  public update(matricula: string, alumno: Alumno): Observable<any> {
    return this.httpClient.put<any>(
      this.alumnoUrl + `editar/${matricula}`,
      alumno
    );
  }

  public delete(matricula: string): Observable<any> {
    return this.httpClient.delete<any>(
      this.alumnoUrl + `eliminar/${matricula}`
    );
  }

  public nuevos(excel: File, idProfesor: number): Observable<any> {
    const file: FormData = new FormData();
    file.append('file', excel);
    return this.httpClient.post<any>(
      this.alumnoUrl + `nuevos/${idProfesor}`,
      file
    );
  }

  public listAll(): Observable<Alumno[]> {
    return this.httpClient.get<Alumno[]>(this.alumnoUrl + `listatodos`);
  }

  public actualizarAlumno(matricula: string, nuevoNombre: string) {
    return this.httpClient.put<any>(
      `${this.alumnoUrl}actualizar/${matricula}/nuevoNombre/${nuevoNombre}`,
      null
    );
  }

  public agregarAlumno(
    idProfesor: number,
    nombreAlumno: string,
    rutAlumno: string
  ) {
    return this.httpClient.post<any>(
      `${this.alumnoUrl}agregar/${idProfesor}/${nombreAlumno}/${rutAlumno}`,
      null
    );
  }
}
