import { Pipe, PipeTransform } from '@angular/core';
import { Alumno } from '../model/alumno';

@Pipe({
  name: 'evaluados'
})
export class EvaluadosPipe implements PipeTransform {

  transform(value: Array<Alumno>, idEvaluacion:number): Array<Alumno> {
    value.forEach(alumno => {
      let evaluado = false;
      alumno.rubricas.forEach(rubrica =>{
        if(rubrica.evaluacion.idEvaluacion == idEvaluacion)
        evaluado = true;
      })
      alumno.evaluado = evaluado;
    })
    return value
  }
}
