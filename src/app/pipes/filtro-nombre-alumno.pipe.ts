import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtroMatriculaAlumno'
})
export class FiltroNombreAlumnoPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const alumnos = [];
    if (value)
      for (let alumno of value) {
        if (alumno.nombreAlumno.indexOf(arg) > -1) {
          alumnos.push(alumno);
        };
      };
    return alumnos;
  }
}
